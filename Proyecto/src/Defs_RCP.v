(*
    Verificacion Formal - Proyecto
    Eduardo Pascual Aseff
    Russian Cards Problem!

    Defs.v - Definiciones utilizadas en el proyecto.
*)
Require Import List.
Require Import Defs_ListSets.

Parameter (Card: Type)
          (A B C: nat)
          (Deck: list Card).

Definition N : nat := A + B + C.

(* Si Alice no tiene cartas, no tiene sentido hablar de anuncios *)
Axiom Alice_has_cards: A > 0.

Axiom Deck_size: length Deck = N.

Axiom Deck_def: forall (c : Card), In c Deck.


Definition Tuple := list Card.

Check @isSet.

(* Una lista de cartas es una mano si no tiene elementos repetidos *)
Definition isHand (tup : Tuple)  := isSet tup.

Check isHand.

Axiom Deck_hand : isHand Deck.

(* Cualquier mano que tenga todas las cartas, es Deck *)
Axiom Deck_equiv: forall (tup : Tuple), 
  isHand tup /\ (forall c : Card, In c tup) -> tup = Deck.


(* Verifica si es una mando de una longitud dada *)
Definition isHandLen (len : nat) (tup : Tuple) : Prop :=
  length tup = len /\ isHand tup.

Definition isAnnouncement (tup: list Tuple): Prop :=
  isSet tup /\ 
  forall (elem : Tuple), In elem tup -> isHandLen A elem.



Definition equalHands(h1 h2: Tuple) : Prop := 
  equalSets h1 h2.




Definition isSafe (ann: list Tuple) : Prop :=
  forall (cH : Tuple), isHandLen C cH -> 
    forall (x: Card), ~ In x cH ->
      (exists (t1 : Tuple),  isHandLen A t1  /\ areDisjoint cH t1 /\ 
                              In x t1 /\ In t1 ann )   /\
      (exists (t2 : Tuple),  isHandLen A t2  /\ areDisjoint cH t2 /\ 
                            ~ In x t2 /\ In t2 ann ).

(* Mi primera definicion de isInformative utilizaba filter,
   pero la cambie a esta para no utilizar definiciones booleanas
 *)
Definition isInformative (ann: list Tuple) : Prop :=
  forall (b : Tuple), (isHandLen B b) -> 
    ~ exists (l1 l2 : Tuple), 
      l1 <> l2 /\ In l1 ann /\ In l2 ann /\ 
      areDisjoint b l1 /\ areDisjoint b l2.

(* Un anuncio es bueno si es informativo y seguro *)
Definition isGood(ann : list Tuple) : Prop :=
  isSafe ann /\ isInformative ann.


























