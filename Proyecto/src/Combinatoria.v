
Require Import Coq.Arith.Compare_dec.

Fixpoint Comb (n m : nat) :=
  match m with
  | 0 => 1
  | S m' => match n with
            | 0 => 0
            | S n' => match lt_eq_lt_dec n m with
                      | inleft (left _) => 0
                      | inleft (right _) => 1
                      | inright _ => Comb (n') (m') + Comb (n') (S m')
                      end
            end
  end.

Compute (Comb 7 3).
Compute (Comb 7 1).
Compute (Comb 7 7).
Compute (Comb 7 8).
