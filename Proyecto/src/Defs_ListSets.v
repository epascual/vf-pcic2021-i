(*
    Verificacion Formal - Proyecto
    Eduardo Pascual Aseff
    Russian Cards Problem!

    Defs_ListSets.v - Sub-biblioteca de conjuntos utilizando listas
*)
Require Import List.
Import ListNotations.
From Coq Require Import Lia.
From Coq Require Import Sorting.Permutation.
From Coq Require Import PeanoNat.


Fixpoint isSet {X : Type} (tup : list X) : Prop :=
  match tup with
  | [] => True
  | h::t => ~ In h t /\ isSet t
  end.


Definition equalSets {X : Type} (h1 h2: list X) : Prop := 
  Permutation h1 h2.

Axiom setsExtensionality: forall {X : Type} (h1 h2 : list X), 
  equalSets h1 h2 -> h1 = h2.


(* Son disjuntas las manos *)
Fixpoint areDisjoint {X : Type} (h1 h2: list X) : Prop :=
  match h1 with
  | [] => True
  | h::t => ~ In h h2 /\ areDisjoint t h2
  end.


(* Conjuntos como listas *)
Definition intersection {X : Type} (h1 h2 inters: list X) : Prop :=
  forall x, In x inters <-> In x h1 /\ In x h2.

Definition union {X : Type} (h1 h2 uni: list X) : Prop :=
  forall x, In x uni <-> In x h1 \/ In x h2.

Definition difference {X : Type} (h1 h2 dif: list X) : Prop :=
  forall x, In x dif <-> In x h1 /\ ~ In x h2.

Definition subset {X : Type} (h1 h2 : list X) : Prop :=
  forall x, In x h1 -> In x h2.
