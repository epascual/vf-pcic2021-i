
Require Import Defs_RCP.
Require Import Defs_ListSets.
Require Import Props_ListSets.
Require Import List.

Import ListNotations.
From Coq Require Import Arith Lia.
From Coq Require Import Sorting.Permutation.
From Coq Require Import Classical.




Lemma unionLE_N: forall (h1 h2 uni : Tuple),
  isSet h1 -> isSet h2 -> isSet uni ->
    union h1 h2 uni -> length uni <= N.
Proof.
  intros h1 h2 uni Hset1 Hset2 HsetU Huni.
  rewrite <- Deck_size.
  apply NoDup_incl_length.
  - apply isSetNoDup_equiv. assumption.
  - unfold incl. intros a _. 
    apply Deck_def.
Qed.

(* Estos dos se usa en varios lugares *)
Lemma B_le_N: B <= length Deck.
Proof. rewrite Deck_size. unfold N. lia. Qed.
Lemma C_le_N: C <= length Deck.
Proof. rewrite Deck_size. unfold N. lia. Qed.


Lemma equalHands_refl: forall (l1 : Tuple),
  equalHands l1 l1.
Proof.
  apply Permutation_refl.
Qed.

Lemma equalHands_sym: forall (l1 l2 : Tuple),
  equalHands l1 l2 -> equalHands l2 l1.
Proof.
  apply Permutation_sym.
Qed. 

Lemma handIncludedInDeck: forall (t1 : Tuple),
  intersection Deck t1 t1.
Proof.
  intros t1.
  unfold intersection.
  split.
  - intros H.
    split.
    + apply Deck_def.
    + assumption.
  - intros [H2 H3].
    assumption.
Qed. 


Lemma existsCardNotIn: forall (t1 : Tuple), 
  (isHand t1 /\ length t1 < length Deck) ->
   exists c : Card, ~ In c t1.
Proof.
  intros t1 [H1 H2].
  apply not_all_ex_not.
  unfold not.
  intros H3.
  assert(H4: t1 = Deck). { apply Deck_equiv. tauto. }
  rewrite H4 in H2.
  contradict H2.
  lia.
Qed.


Lemma existsHandOfLen (n : nat): n <= length Deck ->
  exists (tup: Tuple), isHandLen n tup.
Proof.
  intros H.
  induction n.
  - exists []. 
    split. 
    + reflexivity. 
    + apply I.
  - assert(H1: n <= length Deck). {lia. }
    apply IHn in H1.
    destruct H1 as [t1 H1].
    assert(H2: exists c: Card, ~ In c t1). { 
      apply existsCardNotIn.
      unfold isHandLen in H1. destruct H1 as [H1 H2].
      split.
      + assumption.
      + lia.
    }
    destruct H2 as [c H2].
    exists (c::t1).
    unfold isHandLen in *. 
    destruct H1 as [H1 H3].
    split.
    + simpl. lia.
    + split. 
      { unfold In in H2. intuition. }
      { intuition. }
Qed.


(* Esto no se cumple para cualquier list Tuple,
   solo si es un anuncio conformado por posibles manos de A *)
Lemma LenGE2_twoHandsIn: forall (ann : list Tuple),
  isAnnouncement ann /\ length ann >= 2 ->
    exists l1 l2 : Tuple, 
    l1 <> l2 /\ In l1 ann /\ In l2 ann /\ isHandLen A l1 /\ isHandLen A l2.
Proof.
  intros ann [HAnn HLen].
  destruct ann as [|h1 ann]. 
  { contradict HLen. simpl. lia. }
  { destruct ann as [|h2 ann]. 
    { contradict HLen. simpl. lia. }
    { exists h1. exists h2.
      unfold isAnnouncement in HAnn.
      destruct HAnn as [HSet HAnn].
      split.
      { simpl in HSet. destruct HSet as [HSet _]. 
        unfold not in HSet. contradict HSet. left. rewrite HSet. reflexivity.
      }
      { assert(A1: In h1 (h1 :: h2 :: ann)). 
        { simpl. left. reflexivity. }
        assert(A2: In h2 (h1 :: h2 :: ann)). 
        { simpl. right. left. reflexivity. }
        split. 
        - exact A1.
        - split. 
          + exact A2.
          + split. 
            -- apply HAnn. exact A1.
            -- apply HAnn. exact A2.
      }
    }
  }
Qed.



Lemma existsCardNotIn2: forall (l1 l2 : Tuple),
  isHand l1 -> isHand l2 ->
  length l1 + length l2 < length Deck -> 
    exists (c : Card), ~ In c l1 /\ ~ In c l2.
Proof.
  intros l1 l2 HandL1 HandL2 H.
  assert(A1: exists uni, union l1 l2 uni /\ isHand uni).
  { apply (unionExists); assumption. }
  assert(A2: exists inters, intersection l1 l2 inters /\ isHand inters).
  { apply (intersectionExists); assumption. }
  destruct A1 as [uni [Huni HHandUni]].
  destruct A2 as [inters [Hinters HHandInters]].
  assert(A3: length uni <= length l1 + length l2).
  { rewrite (unionLength l1 l2 uni inters);
      try(assumption); try(lia). 
  }
  assert(A4: exists c, ~ In c uni).
  { apply existsCardNotIn. split. assumption. lia. }
  destruct A4 as [c HNotInUni].
  exists c.
  unfold union in Huni.
  split.
  - contradict HNotInUni. apply <- Huni. left. assumption.
  - contradict HNotInUni. apply <- Huni. right. assumption.
Qed.



Lemma existsDisjoint: forall (l : Tuple) (L : nat), 
  L <= N -> isHand l -> length l <= N - L -> 
   exists l1, isHandLen L l1 /\ areDisjoint l l1.
Proof.
  intros l L.
  generalize dependent l.
  induction L.
  { intros l H1 H2 H3.
    exists [].
    split. 
    - split. 
      + reflexivity. 
      + apply I. 
    - apply disjointComm. apply I. 
  }
  { intros l H1 H2 H3.
    specialize (IHL l).
    assert (A: L <= N). { lia. }
    apply IHL in A.
    * destruct A as [l' [H1' H2']].
      destruct H1' as [H1' H1'']. 
      assert (A2: exists (c: Card), ~In c l /\ ~In c l'). 
      { apply existsCardNotIn2. 
        - assumption. 
        - assumption. 
        - rewrite Deck_size. lia.
      }
      destruct A2 as [c [Hc1 Hc2]].
      exists (c::l').
      split.
      - split.
        { assert(length (c::l') = S (length l')). simpl. lia. lia. }
        { split. assumption. assumption. }
      - apply disjointComm. simpl.
        split. 
        + assumption. 
        + apply disjointComm. assumption.
    * assumption. 
    * lia.
  } 
Qed.


(* Lema auxiliar para uno de los sentidos en informativeAlternative! *)
Lemma lemma1_aux : forall (ann : list Tuple) (l1 l2 uni difDU : Tuple), 
   isAnnouncement ann -> isInformative ann -> 
     l1 <> l2 -> In l1 ann -> In l2 ann ->
     isHand uni -> isHand difDU ->
       union l1 l2 uni -> difference Deck uni difDU ->
       length difDU < B.
Proof.
  intros ann l1 l2 uni difDU.
  intros Hann HInf HDif HIn1 Hin2 HHandUni HHandDifDU HUni HDifDU.
  unfold isInformative in HInf.
  destruct Hann as [Hset Hlens]. 
  assert(HHandL1: isHandLen A l1). 
  { apply Hlens. assumption. }
  assert(HHandL2: isHandLen A l2). 
  { apply Hlens. assumption. }
  destruct HHandL1 as [HLenL1 HHandL1].
  destruct HHandL2 as [HLenL2 HHandL2].
  assert(EQ1: length difDU = length Deck - length uni).
  { apply (diffLength Deck uni difDU uni ). 
    - apply Deck_hand.
    - assumption. - assumption. - assumption. - assumption.
    - apply handIncludedInDeck.
  }
  assert(Contra:= classic (length difDU < B)).
  destruct Contra as [Contra | Contra]. 
  { assumption. }
  { assert (HB: exists b : Tuple, isHandLen B b /\ areDisjoint uni b).
    { apply existsDisjoint.
      - unfold N. lia. 
      - assumption.
      - rewrite Deck_size in EQ1. rewrite EQ1 in Contra.
        assert (length uni <= N). 
        { apply (unionLE_N l1 l2); assumption. }
        lia.
    }
    destruct HB as [b [HHandB HDisj]] .
    specialize (HInf b). 
    apply HInf in HHandB.
    contradict HHandB.
    exists l1. exists l2.
    split. 
    - assumption.
    - split. 
      + assumption.
      + split. 
        * assumption.
        * split. 
          -- apply (disjointUnionLeft l1 l2 uni).
             ++ assumption. 
             ++ apply disjointComm. assumption.
          -- apply (disjointUnionLeft l2 l1 uni).
             ++ apply unionComm. assumption.
             ++ apply disjointComm. assumption.
  }
Qed. 


(* Lemma 1 de Safe_communications... *)
Lemma informativeAlternative: forall (ann : list Tuple), isAnnouncement ann -> 
( isInformative ann <-> 
  forall (l1 l2 inters: Tuple), 
   l1 <> l2 -> In l1 ann -> In l2 ann -> isSet inters ->
     intersection l1 l2 inters -> length (inters) < A - C ).
Proof.
  intros ann Hann.
  assert (Hann' := Hann).
  unfold isAnnouncement in Hann'.
  destruct Hann' as [Hset Hlens]. 
  split.
  { intros HInf l1 l2 inters.
    intros HDif Hin1 Hin2 HsetI Hinter.
    assert(HHandL1: isHandLen A l1). 
    { apply Hlens. assumption. }
    assert(HHandL2: isHandLen A l2).
    { apply Hlens. assumption. }
    destruct HHandL1 as [HL1 HHandL1].
    destruct HHandL2 as [HL2 HHandL2].
    (* unfold isInformative in HInf. *)
    assert(HB: exists b : Tuple, isHandLen B b). 
    { apply (existsHandOfLen B); apply B_le_N. }
    assert(HUni: exists uni, union l1 l2 uni /\ isHand uni).
    { apply unionExists; assumption. }
    destruct HUni as [uni [HUni HHandU]].
    assert(HdifDU: exists difDU, difference Deck uni difDU /\ isHand difDU).
    { apply differenceExists. 
      - apply Deck_hand. 
      - assumption. 
    }
    destruct HdifDU as [difDU [HdifDu HHandDU]].
    assert(H_1: length difDU < B ). 
    { apply (lemma1_aux ann l1 l2 uni difDU); assumption. }
    assert(H_2: length uni = A + A - length inters ).
    { rewrite (unionLength l1 l2 uni inters);
      try(assumption); try(lia).
    }
    assert(H_3: length difDU = A + B + C - (A + A - length inters)).
    { 
      rewrite (diffLength Deck uni difDU uni). 
      - rewrite Deck_size. unfold N. lia.
      - apply Deck_hand.     
      - assumption. - assumption. - assumption. - assumption.
      - apply handIncludedInDeck. 
    }
    rewrite H_3 in H_1.
    lia.
  }
  {
    intros HH.
    intros b HB. 
    apply all_not_not_ex. intros l1.
    apply all_not_not_ex. intros l2.
    apply or_not_and. apply imply_to_or. intros Hl1l2Dif.
    apply or_not_and. apply imply_to_or. intros HInl1.
    apply or_not_and. apply imply_to_or. intros HInl2.
    apply or_not_and. apply imply_to_or. intros HDisjBl1.
    unfold not. intros HDisjBl2.
    assert(Hinter: exists i, intersection l1 l2 i /\ isSet i).
    { apply (intersectionExists l1 l2). 
      - apply Hann. assumption. 
      - apply Hann. assumption.
    }
    destruct Hinter as [inters [Hinters HSetInters]].
    specialize (HH l1 l2 inters).
    assert(HLen: length inters < A - C).
    { apply HH; assumption. }
    assert(HUni: exists uni, union l1 l2 uni /\ isSet uni). 
    { apply unionExists. 
      - apply Hann. assumption. 
      - apply Hann. assumption. 
    }
    destruct HUni as [uni [HUni HSetUni]].
    assert(length l1 + length l2 = length uni + length inters). 
    { apply incExclPrinciple.
      - apply Hann. assumption. 
      - apply Hann. assumption.
      - assumption. - assumption. - assumption. - assumption.
    }
    assert(areDisjoint b uni).
    { apply (disjointUnion b l1 l2); assumption. }
    assert(length l1 = A). 
    { apply Hann; assumption. }
    assert(length l2 = A). 
    { apply Hann; assumption. }
    rewrite H1 in H. rewrite H2 in H.
    assert(HUni': exists uni', union b uni uni' /\ isSet uni'). 
    { apply unionExists. apply HB. assumption. }
    destruct HUni' as [uni' [HUni' HSetUni']].
    assert(length uni' = B + length uni). 
    { assert(exists i, intersection b uni i /\ isSet i).
      { apply intersectionExists. 
        - apply HB. 
        - assumption. 
      }
      destruct H3 as [i [Hi HSeti]].
      assert(i = []).
      { apply (disjointIntersection b uni); assumption. }
      assert(length b + length uni = length uni' + length i).
      { apply (incExclPrinciple ); try(assumption). apply HB. }
      rewrite H3 in H4. simpl in H4. destruct HB. rewrite <- H5. 
      lia. 
    }
    assert(length uni' = B + A + A - length inters). { lia. }
    assert(B + A + A - length uni' < A - C). { lia. }
    assert(A + B + C < length uni'). { lia. }
    assert(length uni' <= N). 
    { apply (unionLE_N b uni). 
      - apply HB. 
      - assumption. - assumption. - assumption.
    }
    assert(A + B + C < N). { lia. }
    unfold N in H8.
    lia.
  }
Qed.



(* Demostrando que un anuncio seguro tiene al menos dos manos *)
Lemma safeAnnounceHasTwoElems: forall (ann: list Tuple), 
  isAnnouncement ann -> isSafe ann -> length ann >= 2.
Proof.
  intros ann Hann Hsafe.
  assert(HC: exists c : Tuple, isHandLen C c).
  { apply existsHandOfLen. apply C_le_N. }
  destruct HC as [c HC].
  assert(HC1 := HC).
  apply Hsafe in HC.
  assert(HX: exists x : Card, ~ In x c).
  { apply existsCardNotIn. 
    split. 
    - apply HC1. 
    - destruct HC1. rewrite H. rewrite Deck_size. unfold N. 
      assert(HA:= Alice_has_cards). 
      lia.
  }
  destruct HX as [x HX].
  specialize (HC x).
  apply HC in HX.
  destruct HX as [[t1 H1] [t2 H2]].
  assert(t1 <> t2).
  { unfold not. intros H. rewrite H in H1. tauto. }
  apply (twoIn_LenGE2 t1 t2); tauto.
Qed.



(* Corollary 1 de Safe_communications.
   Tuve que agregarle la precondicion de que el anuncio tiene dos elementos,
   si no fuese así, es informativo independientemente de C o A
 *)
Corollary corollary1: forall (ann: list Tuple), isAnnouncement ann -> 
  length ann >= 2 -> isInformative ann -> C < A.
Proof.
  intros ann Hann Hlen Hinf.
  assert(Hann' := Hann).
  apply informativeAlternative in Hann.
  destruct Hann as [Hann _].
  assert(Contra:= classic (C < A)).
  destruct Contra as [Contra | Contra]. 
  - assumption. 
  - exfalso.
    assert(exists l1 l2 : Tuple, l1 <> l2 /\ In l1 ann /\ In l2 ann /\ 
                          isHandLen A l1 /\ isHandLen A l2).
    { apply LenGE2_twoHandsIn. tauto. }
    destruct H as [l1 [l2 [H1 [H2 [ H3 [H4 H5]]]]]].
    assert(HI: exists i, intersection l1 l2 i /\ isSet i).
    { apply intersectionExists. 
      - apply H4. 
      - apply H5. 
    }
    destruct HI as [i HI].
    assert(length i < A - C). {
      assert(forall t1 t2 inters : Tuple, t1 <> t2 -> In t1 ann -> In t2 ann ->
             isSet inters -> intersection t1 t2 inters -> length inters < A - C).
      { apply Hann. assumption. }
      apply (H l1 l2).
      - assumption. - assumption. - assumption. 
      - apply HI. - apply HI.
    }
    lia.
Qed.



(* Lemma 2 de Safe_communications... *)
Lemma goodAnnouncemnt_onlyif_cIsLowerThanB: 
  forall (ann: list Tuple), isAnnouncement ann -> isGood ann -> C < B.
Proof.
  intros ann Hann [HSafeAnn HInfAnn].
  assert(Contra:= classic (C < B)).
  destruct Contra as [Contra | Contra]. 
  { assumption. } 
  { exfalso.
    assert(HC: exists c, isHandLen C c).
    { apply existsHandOfLen. apply C_le_N. }
    destruct HC as [c HC].
    assert(HB: exists b, isHandLen B b /\ subset b c).
    { apply (existsSubSet c _ C). 
      - apply HC. 
      - apply HC. 
      - lia. 
    }
    destruct HB as [b [HB HSubset]].
    specialize (HInfAnn b).
    assert(HB' := HB).
    apply HInfAnn in HB'. clear HInfAnn.
    specialize (HSafeAnn c).
    assert(HC' : 
         forall x : Card, ~ In x c ->
         (exists t1 : Tuple,
            isHandLen A t1 /\ areDisjoint c t1 /\ In x t1 /\ In t1 ann) /\
         (exists t2 : Tuple,
            isHandLen A t2 /\ areDisjoint c t2 /\ ~ In x t2 /\ In t2 ann)). 
    {
      apply HSafeAnn. 
      assumption. 
    }
    clear HSafeAnn.
    assert(HX: exists x : Card, ~ In x c). 
    { apply existsCardNotIn.  
      split. 
      - apply HC. 
      - destruct HC. rewrite H. rewrite Deck_size. unfold N. 
        assert(HA:= Alice_has_cards).
        lia.
    }
    destruct HX as [x HX].
    specialize(HC' x). apply HC' in HX.
    destruct HX as [[t1 HX1] [t2 HX2]].
    contradict HB'.
    exists t1. exists t2.
    split. 
    { unfold not.
      intros H.
      rewrite H in HX1.
      tauto.
    }
    { split. 
      - apply HX1.
      - split. 
        + apply HX2.
        + split. 
          * apply (disjointSubset b c t1). assumption. apply HX1.
          * apply (disjointSubset b c t2). assumption. apply HX2.
    }
  }
Qed.



Lemma lemma3_aux1:
  forall (h1 : list Tuple) (x : Card),
    (forall elem : Tuple, In elem h1 -> 
                          isSet elem /\ In x elem /\ length elem > 1) ->
    exists t: Tuple, isSet t /\ ~ In x t /\ length t <= length h1 /\
      forall elem, In elem h1 -> ~ areDisjoint elem t.
Proof.
  intros h1. 
  induction h1.
  { intros x ALLx.
    exists [].
    split. 
    - apply I. 
    - split. 
      + intros H. inversion H.
      + split. 
        * simpl. lia.
        * intros e He. inversion He. 
  }
  { intros x ALLx.
    assert(HInxa: In x a). 
    { apply ALLx. left. reflexivity. }
    assert(HA': exists a', a = x::a'). 
    { apply extractFromSet. assumption. }
    destruct HA' as [a' HA'].
    destruct a'.
    { assert(length a > 1). 
      { apply ALLx. left. reflexivity. }
      rewrite HA' in H. 
      simpl in H. 
      lia.
    } 
    { assert(ALLx': forall elem : Tuple, In elem h1 -> 
                    isSet elem /\ In x elem /\ length elem > 1).
      { intros e He. apply ALLx. right. apply He. }
      specialize (IHh1 x ALLx'). clear ALLx'.
      destruct IHh1 as [t [IH1 [IH2 IH3]]].
      assert(Hseta: isSet a).
      { apply ALLx. left. reflexivity. }
      assert(Casos:= classic (In c t)).
      destruct Casos as [Caso1 | Caso2].
      {
        destruct IH3 as [IH3 IH4]. 
        exists t.
        split. 
        - assumption.
        - split. 
          + assumption.
          + split. 
            * simpl. lia.
            * intros e He. 
              destruct He.
              -- rewrite <- H. rewrite HA'.
                 intros HH. destruct HH. 
                 destruct H1. contradict H1. assumption. 
              -- apply IH4. assumption.
      } 
      {
        exists (c::t).
        split.
        - split; assumption.
        - split.
          + intros H. 
            destruct H.
            * rewrite H in HA'. rewrite HA' in Hseta. 
              destruct Hseta. contradict H0. left. reflexivity.
            * tauto.
          + split. 
            * simpl. lia.
            * intros e He.
              destruct He.
              -- rewrite <- H. rewrite HA'.
                 intros HH. destruct HH. 
                 destruct H1. contradict H1. left. reflexivity.
              -- apply IH3 in H.
                 intros HH. apply disjointComm in HH. destruct HH. 
                 apply disjointComm in H1. tauto.
      }
    }
  }
Qed.



Lemma lemma3_aux2': forall (t: Tuple) (x: Card), 
  isSet t -> length t <= C -> ~ In x t ->
  exists c, isHandLen C c /\ subset t c.
Proof.
  intros t. induction t. 
  { assert(exists c, isHandLen C c). 
    { apply existsHandOfLen. apply C_le_N. }
    destruct H as [c HC].
    assert(Hx': exists x', ~ In x' c). 
    { apply existsCardNotIn. 
      split. 
      - apply HC.
      - destruct HC. 
        rewrite H. 
        assert(HA:= Alice_has_cards). 
        rewrite Deck_size. unfold N. lia.
    }
    destruct Hx' as [x' Hx'].
    intros x HsetT HLent HNotIn_xt.
    assert(Casos:= classic (In x c)).
    destruct Casos as [Caso1|Caso2].
    { assert(HC': exists c', c=x::c'). 
      { apply extractFromSet. assumption. }
      destruct HC' as [c' HC'].
      exists (x'::c').
      split. 
      - split. 
        + destruct HC. rewrite <- H. rewrite HC'. simpl. reflexivity.
        + split. 
          * intros H. contradict Hx'. rewrite HC'. right. assumption.
          * destruct HC. rewrite HC' in H0. destruct H0. assumption.
      - intros e He. inversion He.
    }
    { exists c.
      split. 
      - assumption.
      - intros e He. inversion He.
    }
  }
  {
    intros x HsetT HLent HNotIn_xt.
    assert(HLent': length t <= C). 
    { simpl in HLent. lia. }
    destruct HsetT as [HNotIn_at HSetT].
    specialize (IHt a HSetT HLent' HNotIn_at).
    destruct IHt as [c [HC Hsubset]]. 
    assert(Casos:= classic (In a c)).
    destruct Casos as [Caso1|Caso2].
    { 
      assert(HC': exists c', c=a::c').
      { apply extractFromSet. assumption. }
      destruct HC' as [c' HC'].
      exists (a::c').
      split. 
      - split. 
        + destruct HC. rewrite <- H. rewrite HC'. simpl. reflexivity.
        + split. 
          * intros H. rewrite HC' in HC. destruct HC. destruct H1. tauto.
          * rewrite HC' in HC. apply HC.
      - intros e He. 
        destruct He.
        + rewrite H. left. reflexivity.
        + apply Hsubset in H. rewrite HC' in H. assumption.
    } {
      assert(HLent'': length (a::t) < C \/ length (a::t) = C). 
      { simpl in HLent. simpl. lia. }
      destruct HLent'' as [HLent'' | HLent''].
      { assert(HNotIn_xt': ~ In x t). 
        { contradict HNotIn_xt. right. assumption. }
        assert(exists x', ~ In x' (a::t) /\ In x' c).
        { apply (biggerSetHasOtherElements (a::t) c). 
          - split; assumption. 
          - apply HC. 
          - destruct HC. rewrite H. assumption. 
        }
        destruct H as [x' [Hx'1 Hx'2]].
        assert(exists c', c=x'::c'). 
        { apply extractFromSet. assumption. }
        destruct H as [c' Hc'].
        exists (a::c').
        split. 
        { rewrite Hc' in HC. destruct HC. simpl in H.
          split. 
          - simpl. assumption. 
          - split. 
            + intros HH. contradict Caso2. rewrite Hc'. right. assumption.
            + apply H0.
        }
        { 
          intros e He.
          destruct He. 
          - left. assumption.
          - assert(e <> x').
            { intros HH. 
              contradict Hx'1. right. rewrite <- HH. 
              assumption. 
            }
            apply Hsubset in H.
            rewrite Hc' in H.
            destruct H. 
            + contradict H0. rewrite H. reflexivity.
            + right. assumption.
        }
      }
      { exists (a::t).
        split. 
        - split. 
          + assumption. 
          + split; assumption.
        - intros e He. assumption.
      }
    }
  }
Qed.



Lemma lemma3_aux2: forall (t: Tuple) (x: Card), 
  isSet t -> length t <= C -> ~ In x t ->
  exists c, isHandLen C c /\ subset t c /\ ~ In x c.
Proof.
  intros.
  assert(exists c, isHandLen C c /\ subset t c).
  { apply (lemma3_aux2' t x); assumption. }
  destruct H2 as [c [Hsetc Hsubset]].
  assert(Casos:= classic ( In x c )). 
  destruct Casos as [Caso1|Caso2].
  { assert(exists c', c = x::c').
    { apply extractFromSet. assumption. }
    destruct H2 as [c' Hc'].
    assert(exists x' : Card, ~ In x' c).
    { apply existsCardNotIn.
      split. apply Hsetc.
      destruct Hsetc. rewrite H2. rewrite Deck_size. unfold N.
      assert(HA:= Alice_has_cards).
      lia.
    }
    destruct H2 as [x' Hx'].
    assert(~ In x c').
    { destruct Hsetc. rewrite Hc' in H3. destruct H3. assumption. }
    exists (x'::c').
    rewrite Hc' in Hsetc. destruct Hsetc as [Hsetc1 Hsetc2].
    split.
    - split. 
      + simpl. simpl in Hsetc1. assumption. 
      + split. 
        * intros HH. contradict Hx'. rewrite Hc'. right. assumption.
        * apply Hsetc2.
    - split.
      + intros e He. right.
        assert(He' := He).
        apply Hsubset in He. rewrite Hc' in He.
        destruct He. rewrite H3 in H1. tauto.
        assumption.
      + intros HH. 
        destruct HH.
        * rewrite H3 in Hx'. tauto. 
        * tauto.
  }
  { exists c.
    split. 
    - assumption.
    - split. 
      + assumption.
      + assumption.
  }
Qed.



(* Lemma 3 de Safe_communications... *)
Lemma goodAnnouncement_x_appears_more_than_C_times: A > 1 ->
  forall (ann : list Tuple) (x : Card),
    isAnnouncement ann -> isGood ann ->
    exists (xHands : list Tuple),
      isSet xHands /\
      (forall elem, In elem xHands -> In x elem) /\ 
       subset xHands ann /\ 
       length xHands >= C+1.
Proof.
  intros HLenA ann x Hann [Hsafe Hinf].
  assert(HX: exists (ann_withx ann_nox : list Tuple), 
         isSet ann_withx /\ isSet ann_nox /\union ann_withx ann_nox ann /\
          (forall elem : Tuple, In elem ann_withx ->   In x elem) /\
          (forall elem : Tuple, In elem   ann_nox -> ~ In x elem)).
  { apply setSplit. apply Hann. }
  destruct HX as [ann_withx [ann_nox [Hsetwx [Hsetwnx [Huni [HWX HNX]]]]]].
  exists ann_withx.
  split. 
  { assumption. }
  { split.
    { assumption. }
    { split.
      { unfold union in Huni. unfold subset. 
        intros e1 H. apply <- Huni. left. assumption.
      }
      { assert(H:= classic (length ann_withx >= C + 1)).
        destruct H as [Contra | Contra]. 
        { assumption. }
        { exfalso.
          assert(length ann_withx <= C). { lia. }
          clear Contra. rename H into Contra.
          assert(exists t: Tuple, isSet t /\ ~ In x t /\ 
                                  length t <= length ann_withx /\ 
                                  forall elem, In elem ann_withx -> 
                                  ~ areDisjoint elem t).  
          { apply lemma3_aux1. intros e He.
            assert(In e ann). 
            { apply Huni. left. assumption. }
            split. 
            - apply Hann. assumption.
            - split. 
              + apply HWX. assumption.
              + apply Hann in H. destruct H. rewrite H. assumption.
          } 
          destruct H as [t HT].
          assert(HLenT: length t <= C). 
          { destruct HT as [_ [_ [HT _]]].  lia. }
          assert(HC: exists c, isHandLen C c /\ subset t c /\ ~ In x c). 
          { apply lemma3_aux2. 
            - apply HT. 
            - assumption. 
            - apply HT. 
          }
          destruct HC as [c [HC [HSubsetTC HInC]]].
          specialize (Hsafe c HC x HInC).
          destruct Hsafe as [[t1 Hsafe1] _].
          destruct Hsafe1 as [HLent1 [HDisjCT1 [HInxt1 HInt1ann]]].
          assert(HH: In t1 ann_withx).
          { apply Huni in HInt1ann. 
            destruct HInt1ann. 
            - assumption. 
            - specialize (HNX t1 H). tauto.
          }
          destruct HT as [HT0 [HT1 [HT2 HT3]]].
          specialize (HT3 t1 HH).
          contradict HT3.
          apply disjointComm.
          apply (disjointSubset t c t1). 
          - assumption.
          - assumption.
        }
      }
    }
  }
Qed.








Fixpoint numberOfCardsInAnn (ann : list Tuple) : nat :=
  match ann with
  | [] => 0
  | h1::t1 => length h1 + numberOfCardsInAnn t1
  end.

Lemma numberOfCardsInAliceAnn: forall (ann : list Tuple),
  isAnnouncement ann -> numberOfCardsInAnn ann = length ann * A.
Proof.
  induction ann.
  - reflexivity.
  - intros.
    simpl (numberOfCardsInAnn (a :: ann)).
    assert(length a = A).
    { apply H. left. reflexivity. }
    rewrite H0.
    rewrite IHann. 
    { simpl. lia. }
    split. 
    + apply H.
    + destruct H. intros. apply H1. right. assumption.
Qed.

(* 
 Theorem dec_eq_Card: forall (a b:Card), {a = b} + {a <> b}.
Proof. Admitted.

Fixpoint cardInHand (c : Card) (hand : Tuple) : nat :=
  match hand with
  | [] => 0
  | h1::t1 => match c = h1 with 
              | True => 1
              | _ => 0
              end
  end.

Fixpoint occurrenncesOfCardInAnn (c : Card) (ann : list Tuple) : nat :=
  match ann with
  | [] => 0
  | h1::t1 =>   cardInHand c h1
              + occurrenncesOfCardInAnn c t1
  end.

Fixpoint numberOfCardsInAnn' 
           (cards : list Card) (ann : list Tuple) : nat :=
  match cards with
  | [] => 0
  | c1::t1 =>   occurenncesOfCardInAnn c1 ann
              + numberOfCardsInAnn' t1 ann
  end. *)



(* Definition splittedAnnByCard (x : Card) (ann xHands noxHands: list Tuple) :=
  isSet ann /\ isSet xHands /\ isSet noxHands /\ union xHands noxHands ann /\
          (forall elem : Tuple, In elem  xHands  ->   In x elem) /\
          (forall elem : Tuple, In elem noxHands -> ~ In x elem).

Definition occurrencesOfCardInAnn (c : Card) (ann: list Tuple) : nat :=
  length xHands. *)




Lemma goodAnnouncementMinNumOfCards: A > 1 ->
  forall (ann : list Tuple),
    isAnnouncement ann -> isGood ann ->
    numberOfCardsInAnn ann >= (N)*(C+1).
Proof.
  intros HA.
  assert(A1:= goodAnnouncement_x_appears_more_than_C_times HA).
  intros.
Admitted.

(* Proposition 1 de Safe_communications...  *)
Proposition goodAnnouncementMinLength: A > 1 ->
  forall (ann : list Tuple), 
    isAnnouncement ann -> isGood ann ->
    length ann >= (N)*(C+1)/A.
Proof.
  intros HA ann Hann [Hsafe Hinf].
  assert(HA': A <> 0). 
  { lia. }
  assert(numberOfCardsInAnn ann = length ann * A).
  { apply numberOfCardsInAliceAnn. assumption. }
  assert(A1 : numberOfCardsInAnn ann >= N * (C + 1)).
  { apply goodAnnouncementMinNumOfCards. 
    - assumption.
    - assumption.
    - split. 
      + assumption.
      + assumption.
  }
  rewrite H in A1.
  unfold ge in *. 
  assert(N * (C + 1)/A <= length ann * A / A).
  { apply Nat.div_le_mono. 
    - assumption.
    - assumption. 
  }
  rewrite Nat.div_mul in H0.
  - assumption.
  - assumption.
Qed.
  
  
  





