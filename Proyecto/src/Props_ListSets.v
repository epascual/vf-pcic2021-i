(*
    Verificacion Formal - Proyecto
    Eduardo Pascual Aseff
    Russian Cards Problem!

    Auxiliar.v - Lemas principalmente relacionados 
                 con la teoría de conjuntos
*)

Require Import List.
Require Import Defs_ListSets.
Import ListNotations.
From Coq Require Import Arith Lia.
From Coq Require Import Classical.
From Coq Require Import Sorting.Permutation.


Lemma different_sym: forall {X: Type} (x y: X), x<>y -> y<>x.
Proof. 
  intros X x y H H1. 
  rewrite H1 in H. 
  contradict H. 
  reflexivity. 
Qed.


(* 
   Si hay dos elementos distintos en una lista,
   su longitud es mayor o igual a 2! 
*)
Lemma twoIn_LenGE2: 
  forall {X : Type} (l1 l2 : X) (l : list X),
  l1 <> l2 -> In l1 l -> In l2 l -> length l >= 2.
Proof.
  intros X l1 l2 l H1 H2 H3.
  destruct l.
  { inversion H2. }
  { simpl.
    unfold ge.
    apply le_n_S.
    destruct l.
    - unfold In in H2.
      unfold In in H3.
      destruct H2 as [H2|H2].
      + rewrite H2 in H3.
        destruct H3 as [H3|H3]; intuition.
      + intuition.
    - simpl. lia.
  }
Qed.


(* 
   Equivalencia entre isSet y 
   NoDup de https://coq.inria.fr/library/Coq.Lists.List.html#NoDup
*)
Lemma isSetNoDup_equiv: forall {X: Type} (l : list X),
  isSet l <-> NoDup l.
Proof.
  induction l.
  { split.
    - intros H. apply NoDup_nil.
    - intros H. apply I.
  }
  {
    split.
    - intros H. destruct H. 
      apply NoDup_cons.
      -- assumption. 
      -- apply IHl. assumption.
    - intros H. 
      split. 
      -- inversion H. 
         assumption. 
      -- apply <- IHl. 
         inversion H. 
         assumption.
  }
Qed.



(* *) 
(* Lemas de existencia de la union, interseccion y diferencia *)
(* *) 
Lemma unionExists: forall {X : Type} (h1 h2 : list X),
  isSet h1 -> isSet h2 ->
  exists uni : list X, union h1 h2 uni /\ isSet uni.
Proof.
  intros X h1 h2 Hh1 Hh2.
  induction h1 as [| h t HI].
  { exists h2.
    split.
    { intros x. split. 
      - intros H1. right. assumption.
      - intros [H1|H1]. 
        -- inversion H1. 
        -- assumption.
    } 
    { assumption.
    }
  }
  { destruct Hh1 as [nHT Hh1].
    assert(HT := Hh1).
    apply HI in Hh1.
    destruct Hh1 as [u [Hh1 Hhu]].
    assert(A := classic (In h h2)). 
    destruct A as [A|A].
    {
      exists u.
      unfold union in *.
      split.
      { intros x. simpl.
        split.
        {
          intros H1.
          apply Hh1 in H1.
          destruct H1 as [H1|H1].
          - left. right. assumption.
          - right. assumption.
        }
        {
          intros [[H1|H1]|H1].
          - rewrite H1 in A. apply Hh1. right. assumption.
          - apply Hh1. left. assumption.
          - apply Hh1. right. assumption.
        }
      }
      { assumption. }
    }
    {
      exists (h::u).
      unfold union in *.
      split.
      { intros x. simpl.
        split.
        {
          intros [H1|H1].
          - left. left. assumption.
          - apply Hh1 in H1.
            destruct H1 as [H1|H1].
            + left. right. assumption.
            + right. assumption.
        }
        {
          intros [[H1|H1]|H1].
          - left. assumption.
          - right. apply <- Hh1. left. assumption.
          - right. apply <- Hh1. right. assumption.
        }
      }
      { split.
        - intros H.
          apply Hh1 in H. 
          tauto.
        - assumption.
      }
    }
  }
Qed.



Lemma intersectionExists: forall {X : Type} (h1 h2 : list X),
  isSet h1 -> isSet h2 ->
  exists inters : list X, intersection h1 h2 inters /\ isSet inters.
Proof.
  intros X h1 h2 Hh1 Hh2.
  induction h1 as [| h t HI].
  { exists [].
    unfold intersection.
    split.
    + intros x. 
      split.
      - intros H. inversion H.
      - intros [H1 H2]. inversion H1.
    + assumption.
  }
  {
    destruct Hh1 as [nHT Hh1].
    assert(HT:= Hh1).
    apply HI in Hh1.
    destruct Hh1 as [i [Hh1 Hhu]].
    assert(A:= classic (In h h2)).
    destruct A as [A | A].
    { exists (h::i).
      unfold intersection in *.
      split.
      { intros x.
        split.
        { intros H1.
          inversion H1.
          + split. unfold In. left. assumption. rewrite <- H. assumption.
          + apply Hh1 in H. 
            destruct H as [H2 H3]. 
            split. 
            ++ unfold In. right. assumption. 
            ++ assumption. 
        }
        { intros [H1 H2].
          inversion H1.
          + rewrite H. left. reflexivity.
          + right. apply <- Hh1. split; assumption. 
        }
      }
      { split.
        + unfold not.
          intros H.
          apply Hh1 in H. tauto. 
        + assumption.
      }
    }
    { exists i.
      unfold intersection in *.
      split.
      { intros x.
        split.
        - intros H. apply Hh1 in H. destruct H as [H1 H2].
          split. 
          -- right. assumption. 
          -- assumption.
        - intros [H1 H2].
          apply <- Hh1.
          split. 
          -- inversion H1. 
             + contradict A. rewrite H. assumption.
             + assumption.
          -- assumption.
      }
      { assumption. }
    }
  }
Qed.



(* Este se diferencia al anterior en que no tiene como precondiciones
   que h1 y h2 sean conjuntos.
*)
Lemma intersectionExists': forall {X: Type} (h1 h2 : list X),
  exists inters : list X, intersection h1 h2 inters.
Proof.
  intros X h1 h2.
  induction h1 as [| h t HI].
  { exists [].
    unfold intersection.
    split. 
    { intros H. inversion H. }
    { intros [H1 H2]. inversion H1. }
  }
  {
    destruct HI as [i Hh1].
    assert(A:= classic (In h h2)).
    destruct A as [A | A].
    { exists (h::i).
      unfold intersection in *.
      intros x.
      split.
      { intros H1.
        inversion H1.
        + split. 
          ++ unfold In. left. assumption. 
          ++ rewrite <- H. assumption.
        + apply Hh1 in H. 
          destruct H as [H2 H3]. 
          split. 
          ++ unfold In. right. assumption. 
          ++ assumption. 
      }
      { intros [H1 H2].
        inversion H1.
        + rewrite H. left. reflexivity.
        + right. apply <- Hh1. split; assumption. 
      }
    }
    { exists i.
      unfold intersection in *.
      intros x. split.
      { 
        split.
        - apply Hh1 in H. destruct H as [H1 H2].
          right. assumption. 
        - apply Hh1 in H. destruct H. assumption.
      }
      { intros [H1 H2].
        apply <- Hh1.
        split. 
        - destruct H1. 
          -- rewrite H in A. tauto.
          -- assumption. 
        - assumption.
      }
    }
  }
Qed.



Lemma differenceExists: forall {X: Type} (h1 h2 : list X),
  isSet h1 -> isSet h2 ->
  exists diff : list X, difference h1 h2 diff /\ isSet diff.
Proof.
  intros X h1 h2 Hh1 Hh2.
  induction h1 as [| h t HI].
  { exists [].
    unfold difference.
    split.
    - intros x. 
      split.
      + intros H. inversion H.
      + intros [H1 H2]. inversion H1.
    - assumption.
  }
  {
    destruct Hh1 as [nHT Hh1].
    assert(HT:= Hh1).
    apply HI in Hh1.
    destruct Hh1 as [d [Hh1 Hhu]].
    assert(A:= classic (In h h2)).
    destruct A as [A | A].
    {
      exists d.
      split.
      { unfold difference in *.
        intros x.
        split.
        {
          intros H.
          apply Hh1 in H. destruct H as [H1 H2].
          split. 
          - right. assumption. 
          - assumption.
        }
        {
          intros [H1 H2].
          apply <- Hh1.
          split.
          - destruct H1 as [H1|H1].
            -- contradict H2. rewrite <- H1. assumption.
            -- assumption.
          - assumption.
        }
      }
      { assumption. }
    }
    {
      exists (h::d).
      unfold difference in *.
      split.
      { intros x.
        split.
        {
          intros H.
          split.
          { destruct H.
            - left. assumption. 
            - right. apply Hh1 in H. tauto. 
          } 
          { destruct H.
            - rewrite <- H. assumption.
            - apply Hh1 in H. tauto.
          }
        }
        {
          intros [H1 H2].
          destruct H1.
          - left. assumption.
          - right. apply <- Hh1. tauto.
        }
      }
      split.
      { unfold not.
        intros H.
        apply Hh1 in H.
        tauto.
      }
      { assumption. }
    }
  }
Qed.
(* *) 
(* Fin de lemas de existencia! *)
(* *) 


(* 
    Si un elemento c esta en un conjunto l, entonces existe
    otro l' al que al agregarle c obtenemos l
*)
Lemma extractFromSet: forall {X: Type} (l : list X) (c : X),
  In c l -> exists l', l = c::l'.
Proof.
  intros X l c H.
  induction l.
  { contradict H. }
  { assert(H0:= classic (c = a)).
    destruct H0.
    { exists l. rewrite H0. reflexivity. }
    { assert (In c l). 
      { destruct H. 
        - contradict H.  apply different_sym. assumption. 
        - assumption.
      }
      apply IHl in H1.
      destruct H1 as [l1 H1].
      rewrite H1.
      exists (a::l1).
      apply setsExtensionality.
      apply perm_swap.
    }
  }
Qed.


(* 
  Dos conjuntos son disjuntos si y solo si su interseccion es vacía 
*)
Lemma disjointIntersection: forall {X: Type} (h1 h2 inter: list X),
  intersection h1 h2 inter -> areDisjoint h1 h2 <-> inter = [].
Proof.
  intros X h1 h2 inter H.
  split.
  { intros H1.
    assert(H0:= classic(exists c, In c inter) ).
    destruct H0 as [[c H0]|H0].
    { apply H in H0. 
      destruct H0.
      assert(A1: exists h1', h1 = c::h1'). 
      { apply extractFromSet. assumption. }
      assert(A2: exists h2', h2 = c::h2'). 
      { apply extractFromSet. assumption. }
      destruct A1 as [h1' A1].
      destruct A2 as [h2' A2].
      rewrite A1 in H1. rewrite A2 in H1.
      contradict H1.
      unfold not. intros H3.
      destruct H3. contradict H1. 
      left. reflexivity.
    } 
    { destruct inter. 
      - reflexivity.
      - contradict H0.
        exists x. 
        left. reflexivity.
    }
  }
  {
    intros H1. rewrite H1 in H.
    induction h1.
    { apply I. }
    { split.
      + unfold not. intros H2.
        unfold intersection in H. 
        assert(In a []).  
        { apply <- H. 
          split. 
          - left. reflexivity. 
          - assumption. 
        }
        inversion H0.
      + apply IHh1.
        unfold intersection.
        intros x.
        split. 
        ++ intros H2. inversion H2.
        ++ intros [H2 H3].
           assert(In x []).
           { unfold intersection in H. apply <- H.
             split. 
             - right. assumption. 
             - assumption.
           }
           inversion H0.
    }
  }
Qed.




(* *) 
(* Conmutatividad de operaciones entre conjuntos *) 
(* *)
Lemma intersectionComm: forall {X: Type} (h1 h2 inters: list X),
  intersection h1 h2 inters -> intersection h2 h1 inters.
Proof.
  intros X h1 h2 inters.
  unfold intersection.
  intros H x.
  split.
  - intros H1. apply H in H1. tauto.
  - intros H1. apply <- H. tauto.
Qed.

Lemma disjointComm: forall {X: Type} (h1 h2: list X),
  areDisjoint h1 h2 -> areDisjoint h2 h1.
Proof.
  intros X h1 h2 H.
  assert(A1: exists i, intersection h1 h2 i).
  { apply intersectionExists'. }
  destruct A1 as [i Hi].
  assert(i = []).
  { apply (disjointIntersection h1 h2); assumption. }
  rewrite H0 in Hi.
  apply intersectionComm in Hi.
  apply <- (disjointIntersection h2 h1 []).
  - reflexivity. 
  - assumption.
Qed.

Lemma unionComm: forall {X: Type} (h1 h2 u: list X), 
  union h1 h2 u -> union h2 h1 u.
Proof.
  intros X h1 h2 u.
  unfold union.
  intros H x.
  split.
  - intros H1. apply H in H1. tauto.
  - intros H1. apply <- H. tauto.
Qed.

(* *) 
(* Fin - Conmutatividad *) 
(* *)





(* Differencia [] _ = [] *)
Lemma difference_nil_left: forall {X: Type} (h1 dif: list X), 
  difference [] h1 dif -> dif = [].
Proof.
  intros X h1 dif H.
  unfold difference in H.
  destruct dif. 
  - reflexivity.
  - assert(A: In x []).
    { apply H. left. reflexivity. }
    inversion A.
Qed.

(* Differencia H1 _ = H1 *)
Lemma difference_nil_right: forall {X: Type} (h1 dif: list X), 
  isSet h1 -> isSet dif ->
  difference h1 [] dif -> dif = h1.
Proof.
  intros X h1 dif HSet1 HSet2 H.
  unfold difference in H.
  destruct h1. 
  - destruct dif.
    + reflexivity.
    + assert(In x []). { apply H. left. reflexivity. }
      inversion H0.
  - assert(forall y : X, In y dif <-> In y (x :: h1)).
    { intros y. 
      split. 
      - intros H1. apply H in H1. tauto.
      - intros H1. apply <- H. tauto.
    }
    apply setsExtensionality.
    apply NoDup_Permutation.
    + apply isSetNoDup_equiv. assumption.
    + apply isSetNoDup_equiv. assumption.
    + assumption.
Qed.

(* Intersecction [] _ = [] *)
Lemma intersection_nil_left: forall {X: Type} (h1 inters: list X), 
  intersection [] h1 inters -> inters = [].
Proof.
  intros X h1 inters H.
  unfold intersection in H.
  destruct inters. 
  - reflexivity.
  - assert(A: In x []).
    { apply H. left. reflexivity. }
    inversion A.
Qed.

(* Union [] X = X *)
Lemma union_nil_left: forall {X: Type} (h1 uni: list X), 
  union [] h1 uni -> isSet h1 -> isSet uni -> h1 = uni.
Proof.
  intros X h1 uni H HS1 HS2.
  unfold union in H.
  destruct h1. 
  { destruct uni. 
    - reflexivity.
    - assert(H0: In x (x::uni)). { left. reflexivity. }
      assert(H1: In x []). 
      { apply H in H0. 
        destruct H0; inversion H0.
      }
      inversion H1.
  } 
  {
    apply setsExtensionality. 
    apply NoDup_Permutation. 
    - apply isSetNoDup_equiv. assumption.
    - apply isSetNoDup_equiv. assumption.
    - intros y. split.
      -- intros H1. apply <- H. right. assumption.
      -- intros H1. apply H in H1. 
         destruct H1. 
         --- inversion H0. 
         --- assumption.
  } 
Qed.




(* Principio de inclusion-exclusion *)
Lemma incExclPrinciple: forall {X: Type} (h1 h2 inters uni : list X),
  isSet h1 -> isSet h2 -> isSet inters -> isSet uni ->
    intersection h1 h2 inters -> union h1 h2 uni ->
    length h1 + length h2 = length uni + length inters.
Proof.
  intros X h1.
  induction h1.
  {
    intros. 
    assert(inters = []). 
    { apply (intersection_nil_left h2). assumption. }
    rewrite H5.
    assert(h2 = uni). 
    { apply (union_nil_left); assumption. }
    rewrite H6. 
    lia.
  }
  {
    intros h2 inters uni Hset1 Hset2 HsetI HsetU Hinters Huni.
    assert (A:= classic (In a h2)).
    destruct A as [A|A].
    { assert(A1: union h1 h2 uni).
      { unfold union in Huni. unfold union.
        intros x. 
        split.
        - intros H. apply Huni in H. 
          destruct H. 
          -- destruct H. 
             --- right. rewrite <- H. assumption.
             --- left. assumption.
          -- right. assumption.
        - intros H. apply <- Huni. 
          destruct H.
          -- left. right. assumption.
          -- right. assumption.
      }
      assert(A2: In a inters). 
      { apply Hinters. 
        split. 
        - left. reflexivity. 
        - assumption. 
      }
      assert(A3: exists i, inters = a::i).
      { apply extractFromSet. assumption. }
      destruct A3 as [inters' Hinters'].
      assert(A3: intersection h1 h2 inters').
      { unfold intersection in Hinters. 
        intros x. 
        split. 
        { intros H. 
          assert(x <> a). {
            rewrite Hinters' in HsetI. unfold isSet in HsetI.
            destruct HsetI. intros HH. contradict H. rewrite HH. 
            assumption.
          }          
          assert (In x inters). 
          { rewrite Hinters'. right. assumption. }
          apply Hinters in H1.
          destruct H1 as [[H1|H1] H2].
          - contradict H0. rewrite H1. reflexivity.
          - tauto.
        }
        { intros [H1 H2].
          assert(x <> a). {
            destruct Hset1. intros HH. contradict H. 
            rewrite <- HH. assumption.
          }
          assert(In x inters).
          { apply Hinters. 
            split. 
            - right. assumption. 
            - assumption. 
          }
          rewrite Hinters' in H0.
          destruct H0.
          - contradict H. rewrite H0. reflexivity.
          - assumption.
        }
      }
      assert(A4: length h1 + length h2 = length uni + length inters'). {
        apply IHh1. 
        - apply Hset1. 
        - assumption.
        - rewrite Hinters' in HsetI. apply HsetI. 
        - assumption. 
        - assumption. 
        - assumption.
      }
      rewrite Hinters'.
      simpl. lia.
    }
    { assert(A1: In a uni).
      { apply Huni. left. left. reflexivity. }
      assert(A2: exists u, uni = a::u).
      { apply extractFromSet. assumption. }
      destruct A2 as [uni' A2].
      assert(A3: union h1 h2 uni').
      { intros x. unfold union in Huni.
        split.
        { intros H. 
          assert(In x uni). 
          { rewrite A2. right. assumption. }
          apply Huni in H0. 
          destruct H0.
            - destruct H0.
              -- rewrite A2 in HsetU.
                 destruct HsetU. contradict H1. 
                 rewrite H0. assumption.
              -- left. assumption.
            - right. assumption.
        }
        { intros [H1|H2].
          { assert(In x uni). 
            { apply <- Huni. left. right. assumption. }
            rewrite A2 in H.
            destruct H. 
            - destruct Hset1. contradict H0. rewrite H. assumption.
            - assumption.
          }
          { assert(In x uni). 
            { apply <- Huni. right. assumption. }
            rewrite A2 in H.
            destruct H. 
            - contradict A. rewrite H. assumption.
            - assumption. 
          }
        }
      }
      assert(A4: intersection h1 h2 inters).
      { unfold intersection in Hinters.
        intros x. 
        split.
        { intros H. 
          apply Hinters in H. 
          destruct H as [H1 H2].
          split.
          - destruct H1. 
            -- contradict A. rewrite H. assumption.
            -- assumption.
          - assumption.
        }
        { intros [H1 H2].
          apply <- Hinters.
          split. 
          - right. assumption. 
          - assumption.
        }
      }
      assert(A5: length h1 + length h2 = length uni' + length inters). {
        apply IHh1.
        - apply Hset1. 
        - assumption.
        - assumption.
        - rewrite A2 in HsetU. apply HsetU.
        - assumption.
        - assumption.
      }
      rewrite A2. simpl. lia.
    }
  }
Qed.


(* Longitud de la diferencia de conjuntos (auxiliar) *)
Lemma diffLength': forall {X: Type} (h1 h2 dif inters : list X),
  isSet h1 -> isSet h2 -> isSet dif -> isSet inters ->
  difference h1 h2 dif -> intersection h1 h2 inters ->
    length dif + length inters = length h1.
Proof.
  intros X h1.
  induction h1.
  { intros h2 dif inters.
    intros SetH1 SetH2 SetDif SetInters HDiff HInters.
    rewrite (difference_nil_left h2 dif).
    - rewrite (intersection_nil_left h2 inters). 
      -- simpl. reflexivity. 
      -- assumption. 
    - assumption.
  }
  { intros h2 dif inters.
    intros Hset1 Hset2 HsetD HsetI Hdiff Hinters.
    assert (A:= classic (In a h2)).
    destruct A as [A|A].
    { assert(A1: difference h1 h2 dif).
      {
        intros x. unfold difference in Hdiff.
        split. 
        - intros H. 
          apply Hdiff in H.
          destruct H as [H1 H2].
          split.
          -- destruct H1.
             --- contradict H2. rewrite <- H. assumption.
             --- assumption.
          -- assumption.
        - intros [H1 H2].
          apply <- Hdiff.
          split. 
          -- right. assumption. 
          -- assumption.
      }
      assert(A2: In a inters). 
      { apply Hinters. 
        split. 
        - left. reflexivity. 
        - assumption. 
      }
      assert(A3: exists i, inters = a::i).
      { apply extractFromSet. assumption. }
      destruct A3 as [inters' Hinters'].
      assert(A3: intersection h1 h2 inters').
      { unfold intersection in Hinters. 
        intros x. 
        split. 
        { intros H. 
          assert(x <> a). {
            rewrite Hinters' in HsetI. unfold isSet in HsetI.
            destruct HsetI. intros HH. contradict H. rewrite HH. 
            assumption.
          }
          assert (In x inters). 
          { rewrite Hinters'. right. assumption. }
          apply Hinters in H1.
          destruct H1 as [[H1|H1] H2].
          - contradict H0. rewrite H1. reflexivity.
          - tauto.
        }
        { intros [H1 H2].
          assert(x <> a). {
            destruct Hset1. intros HH. contradict H. 
            rewrite <- HH. assumption.
          }
          assert(In x inters).
          { apply Hinters. 
            split. 
            - right. assumption. 
            - assumption.
          }
          rewrite Hinters' in H0.
          destruct H0.
          - contradict H. rewrite H0. reflexivity.
          - assumption.
        }
      }
      assert(A4: length dif +  length inters' = length h1).
      { apply (IHh1 h2).
        - apply Hset1.
        - assumption.
        - assumption.
        - rewrite Hinters' in HsetI. apply HsetI.
        - assumption.
        - assumption.
      }
      rewrite Hinters'. simpl. lia.
    }
    { (*Caso 2: ~ In a h2 *)
      unfold difference in Hdiff. 
      assert(A1: In a dif). { 
        apply <- Hdiff. 
        split. 
        - left. reflexivity. 
        - assumption.
      }
      assert(A2: exists d, dif = a::d). 
      { apply extractFromSet. assumption. }
      destruct A2 as [dif' A2].
      assert(A3: difference h1 h2 dif').
      { intros x.
        split.
        { intros H. 
          assert(In x dif). 
          { rewrite A2. right. assumption. }
          apply Hdiff in H0.
          rewrite A2 in HsetD.
          destruct H0 as [H1 H2].
          destruct H1.
          - destruct HsetD. contradict H1. rewrite H0. assumption.
          - split;assumption.
        }
        { intros [H1 H2].
          assert(In x dif). 
          { apply <- Hdiff. 
            split. 
            - right. assumption. 
            - assumption. 
          }
          rewrite A2 in H. 
          destruct H.
          - destruct Hset1. contradict H0. rewrite H. assumption. 
          - assumption. 
        }
      }
      assert(A4: intersection h1 h2 inters).
      { unfold intersection in Hinters.
        intros x. 
        split.
        { intros H. apply Hinters in H. destruct H as [H1 H2].
          split.
          - destruct H1. 
            -- contradict A. rewrite H. assumption.
            -- assumption.
          - assumption.
        }
        { intros [H1 H2].
          apply <- Hinters.
          split. 
          - right. assumption. 
          - assumption.
        }
      }
      assert(A5: length dif' + length inters = length h1).
      { apply (IHh1 h2).
        - apply Hset1.
        - assumption.
        - rewrite A2 in HsetD. apply HsetD.
        - assumption.
        - assumption.
        - assumption.
      }
      rewrite A2. 
      simpl. lia.
    }
  }
Qed.

(* Longitud de la deferencia de dos conjuntos *)
Lemma diffLength: forall {X: Type} (h1 h2 dif inters : list X),
  isSet h1 -> isSet h2 -> isSet dif -> isSet inters ->
  difference h1 h2 dif -> intersection h1 h2 inters ->
    length dif = length h1 - length inters.
Proof.
  intros X h1 h2 dif inters Hset1 Hset2 HsetD HsetI Ddif Hinters.
  assert(length dif + length inters = length h1). 
  { apply (diffLength' h1 h2); assumption. }
  lia.
Qed.

(* Longitud de la union de dos conjuntos *)
Lemma unionLength: forall {X: Type} (h1 h2 uni inters : list X),
  isSet h1 -> isSet h2 -> isSet uni -> isSet inters ->
  union h1 h2 uni -> intersection h1 h2 inters ->
    length uni = length h1 + length h2 - length inters.
Proof. 
  intros X h1 h2 uni inters Hset1 Hset2 HsetU HsetI Huni Hinters.
  assert(length h1 + length h2 = length uni + length inters).
  { apply (incExclPrinciple); assumption. }
  lia.
Qed.






(* Si d es disjunto a la union de h1 y h2, también es disjunto
   con respecto a h1 *)
Lemma disjointUnionLeft: forall {X: Type} (h1 h2 uni d: list X), 
  union h1 h2 uni -> areDisjoint d uni -> areDisjoint d h1.
Proof.
  intros X h1 h2 uni d.
  intros H1 H2.
  induction h1.
  { induction d.
    - apply I.
    - split.
      -- simpl. tauto.
      -- apply IHd.
         destruct H2. assumption.
  }
  {
    induction d.
    { apply I. }
    { split. 
      { destruct H2 as [H2 H2']. unfold not.
        intros H. contradict H2.
        unfold union in H1.
        apply <- H1.
        left. assumption.
      }
      {
        apply IHd. 
        - destruct H2 as [H2 H2']. assumption.
        - intros H. apply IHh1 in H. destruct H. assumption.
      }
    }
  }
Qed.


Lemma disjointUnion: forall {X: Type} (b h1 h2 uni : list X),
  areDisjoint b h1 -> areDisjoint b h2 ->
  union h1 h2 uni -> areDisjoint b uni.
Proof.
  intros X b h1 h2 uni HDJ1 HDJ2 HUni.
  induction b. 
  { apply I. }
  { split.
    - unfold union in HUni.
      intros H. 
      apply HUni in H.
      destruct H.
      + destruct HDJ1 as [Contrad _]. tauto.
      + destruct HDJ2 as [Contrad _]. tauto.
    - apply IHb.
      + destruct HDJ1 as [_ HH]. assumption.
      + destruct HDJ2 as [_ HH]. assumption.
  }
Qed. 


Lemma existsSubSet: forall {X: Type} (h2 : list X) (L1 L2 : nat),
  isSet h2 -> length h2 = L2 -> L1 <= L2 ->
  exists h1, (length h1 = L1 /\ isSet h1) /\ subset h1 h2.
Proof.
  intros X h2. 
  induction h2.
  { intros L1 L2 H1 H2 H3.
    exists []. 
    simpl in H2.
    assert(H4: L1 = 0). { lia. }
    split. 
    - split. 
      -- simpl. lia. 
      -- apply I. 
    - unfold subset. intros x H. assumption.
  }
  { intros L1 L2 H1 H2 H3.
    destruct L1. 
    {
      exists [].
      split. 
      - split. 
        -- simpl. lia. 
        -- apply I.
      - unfold subset. intros x H. inversion H.
    } 
    { destruct H1 as [H1 Hset].
      assert(A: exists L2', L2 = S L2'). 
      { exists (length h2). simpl in H2. lia. }
      destruct A as [L2' H4].
      assert(L1 <= L2'). { lia. }
      specialize (IHh2 L1 L2').
      apply IHh2 in Hset.
      - destruct Hset as [h1 Hset].
        exists (a::h1).
        split. 
        { split. 
          + simpl. apply eq_S. apply Hset.
          + split. 
            ++ destruct Hset. unfold subset in H5. 
               unfold not. intros HH. apply H5 in HH. tauto.
            ++ apply Hset. 
        }
        { unfold subset. intros x HH.
          destruct HH. 
          + rewrite H0. left. reflexivity.
          + right. apply Hset. assumption. 
        }
      - simpl in H2. lia.
      - assumption.
    }
  }
Qed.


Lemma disjointSubset: forall {X: Type} (h1 h2 d: list X),
  subset h1 h2 -> areDisjoint h2 d -> areDisjoint h1 d.
Proof.
  intros X h1 h2 d H1 H2. 
  induction h1.  
  { apply I. }
  { split. 
    { assert(A: In a h2). 
      { apply H1. left. reflexivity. }
      contradict H2. 
      assert(A1: exists h2': list X, h2 = a::h2').
      { apply extractFromSet. assumption. }
      assert(A2: exists d': list X, d = a::d').
      { apply extractFromSet. assumption. }
      destruct A1 as [h2' A1].
      destruct A2 as [d' A2].
      rewrite A1. rewrite A2.
      unfold not.
      intros H. destruct H. contradict H. left. reflexivity.
    }
    { apply IHh1. 
      unfold subset in H1.  unfold subset.
      intros x H. apply H1. right. assumption.
    }
  }
Qed.


(* Un conjunto lo podemos dividir en dos subconjuntos,
   los que tienen un elemento x y los que no *)
Lemma setSplit: forall {X: Type} (ann: list (list X)) (x: X),
  isSet ann -> exists (ann_withx ann_nox : list (list X)), 
    isSet ann_withx /\ isSet ann_nox /\ union ann_withx ann_nox ann /\
    (forall elem : list X, In elem ann_withx ->   In x elem) /\
    (forall elem : list X, In elem   ann_nox -> ~ In x elem).
Proof.
  intros X ann x Hann.
  induction ann.
  { exists []. 
    exists [].
    split. 
    - apply I.
    - split. 
      + apply I.
      + split. 
        -- split. 
           ++ intros H. inversion H. 
           ++ intros H. destruct H as [H|H]; inversion H.
        -- split; intros e H; inversion H.
  }
  {
    destruct Hann as [HInA HsetA].
    specialize (IHann HsetA).
    destruct IHann as [wx [wnx [Hsetwx [Hsetwnx [Huni [Hwx Hwnx]]]]]].
    assert(HA:= classic (In x a)).
    destruct HA as [HA|HA].
    { exists (a::wx). exists wnx.
      split. 
      { split. 
        { intros H. 
          assert(Contra: In a ann). 
          { apply Huni. left. assumption. }
          tauto.
        }
        { assumption. }
      } 
      { split.
        { assumption. }
        { split.
          { split.
            { intros H. 
              destruct H.
              - left. left. assumption.
              - apply Huni in H.
                destruct H.
                + left. right. assumption.
                + right. assumption.
            }
            { intros [H|H].
              - destruct H.
                + left. assumption.
                + right. apply Huni. left. assumption.
              - right. apply Huni. right. assumption.
            }
          }
          { split.
            { intros e He.
              destruct He as [He|He].
              - rewrite <- He. assumption.
              - apply Hwx. assumption.
            }
            { intros e He.
              apply Hwnx. assumption.
            }
          }
        }
      }
    }
    { exists wx. exists (a::wnx).
      split. 
      { assumption. }
      { split. 
        { split.
            intros H.
            assert(Contra: In a ann). apply Huni. right. assumption.
            tauto. 
            assumption.
        }
        { split.
          { split.
            { intros H.
              destruct H.
              - right. left. assumption.
              - apply Huni in H.
                destruct H.
                + left. assumption.
                + right. right. assumption.
            }
            { intros [H|H].
              - right. apply Huni. left. assumption.
              - destruct H.
                + left. assumption.
                + right. apply Huni. right. assumption.
            }
          } 
          { split.
            { intros e He.
              apply Hwx. assumption.
            }
            { intros e He. 
              destruct He.
              - rewrite <- H. assumption.
              - apply Hwnx. assumption.
            }
          }
        }
      }
    }
  }
Qed.



Lemma biggerSetHasOtherElements: forall {X: Type} (h1 h2 : list X),
  isSet h1 -> isSet h2 -> length h1 < length h2 ->
  exists x : X, ~ In x h1 /\ In x h2.
Proof.
  intros X h1 h2.
  generalize dependent h1.
  induction h2.
  { intros h1 _ _ Hlen.
    simpl in Hlen. 
    lia.
  }
  { 
    intros h1 Hset1 Hset2 Hlen. 
    assert(Casos := classic (In a h1)).
    destruct Casos as [Caso|Caso].
    { assert(exists h1', h1=a::h1').
      { apply extractFromSet. assumption. }
      destruct H as [h1' Hh1].
      assert(exists x : X, ~ In x h1' /\ In x h2).
      { apply IHh2.
        - rewrite Hh1 in Hset1. apply Hset1.
        - apply Hset2.
        - rewrite Hh1 in Hlen. simpl in Hlen. lia.
      }
      destruct H as [x [Hx1 Hx2]].
      exists x.
      split.
      - intros H. rewrite Hh1 in H. 
        destruct H. 
        + destruct Hset2. rewrite H in H0. tauto.
        + tauto.
      - right. assumption.
    }
    { exists a. 
      split.
      - assumption.
      - left. reflexivity.
    }
  }
Qed.









