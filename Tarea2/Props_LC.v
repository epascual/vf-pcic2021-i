
(* Tarea 2 - VF *)
(* Eduardo Pascual Aseff *)
(* Seccion de Lógica Clasica *)

Require Import Defs_LC.
Check (cotenabilidad).


(* Ejercicio 3 *)
(* Inciso a, Conmutatividad *)
Lemma cot_comm_aux : forall (A B: Prop), (A ~> B) -> (B ~> A).
Proof.
  intros A B.
  unfold cotenabilidad. 
  intros.
  apply imply_to_and in H.
  destruct H as [HA HB].
  rewrite DefImp.
  rewrite dMorganO.
  split. 
  { exact HB. }
  { apply edn. exact HA. }
Qed.

Theorem cot_comm : forall (A B: Prop), (A ~> B) <-> (B ~> A).
Proof.
  intros A B.
  split.
  - apply cot_comm_aux.
  - apply cot_comm_aux.
Qed.


(* Inciso b, Conmutatividad *)
Theorem cot_assoc : forall (A B C: Prop), 
  (A ~> (B ~> C)) <-> ((A ~> B) ~> C).
Proof.
  intros A B C.
  unfold cotenabilidad in *.
  split.
  { intro H1.
    apply imply_to_and in H1.
    destruct H1 as [HA HBC].
    apply NNPP in HBC.
    apply imply_to_and in HBC.
    destruct HBC as [HB HC].
    rewrite DefImp.
    rewrite dMorganO.
    split.
    { apply edn. 
      rewrite DefImp.
      apply dMorganO.
      split.
      { apply edn. exact HA. }
      { apply edn. exact HB. }
    }
    { exact HC. }
  }
  {
    intro H1.
    apply imply_to_and in H1.
    destruct H1 as [HAB HC].
    apply imply_to_and in HAB.
    destruct HAB as [HA HB].
    rewrite DefImp.
    rewrite dMorganO.
    split.
    { apply edn. exact HA. }
    { apply edn. 
      rewrite DefImp.
      rewrite dMorganO.
      split.
      { exact HB. }
      { exact HC. }
    } 
  }
Qed.


(* Inciso c, Distributividad *)
Theorem cot_dist: forall (A B C : Prop), 
  (A ~> B) \/ (A ~> C) <-> A ~> (B \/ C).
Proof.
  intros A B C.
  unfold cotenabilidad in *.
  repeat(rewrite DefImp).
  split.
  {
    intros H.
    apply <- dMorganO.
    destruct H as [H|H].
    {
      apply dMorganO in H.
      destruct H as [HA HB].
      split.
      - exact HA.
      - apply edn.
        left.
        apply NNPP.
        exact HB.
    }
    {
      apply dMorganO in H.
      destruct H as [HA HC].
      split.
      - exact HA.
      - apply edn.
        right.
        apply NNPP.
        exact HC.
    }
  }
  {
    intros H.
    apply dMorganO in H.
    destruct H as [HA HBC].
    apply NNPP in HBC.
    destruct HBC as [HB|HC].  
    { left. 
      apply dMorganO.  
      split.
      - exact HA.
      - apply edn in HB.
        exact HB.
    }
    {
      right.
      apply dMorganO.  
      split.
      - exact HA.
      - apply edn in HC.
        exact HC.
    }
  }
Qed.


(* Inciso d, Fusion *)
Theorem cot_fusion: forall (A B C : Prop), 
  (A -> B -> C) <-> (A ~> B -> C).
Proof.
  intros A B C.
  unfold cotenabilidad.
  repeat (rewrite (DefImp)).
  split.
  {
    intros H.
    apply or_assoc in H.
    rewrite <- (edn (~ A \/ ~ B)) in H.
    exact H.
  }
  {
    intros H.
    rewrite (edn (~ A \/ ~ B)) in H.
    apply or_assoc in H.
    exact H.
  }
Qed.


(* Inciso e, Definicion de implicacion *)
Theorem cot_DefImp: forall (A B : Prop),
  (A -> B) <-> ~(A ~> ~B).
Proof.
  intros A B.
  unfold cotenabilidad.
  rewrite (edn (A -> ~ ~ B)).
  rewrite (edn (B)).
  split.
  - intros H. exact H.
  - intros H. exact H.
Qed.




(* Ejercicio 4 *)
(* Inciso a *)
Theorem Ej4_IncA: 
  forall (T : Type) (A B C : T -> Prop),
  ((exists x : T, (A x /\ B x)) -> (forall x : T, B x -> C x)) ->
  (exists a : T, (B a /\ ~ C a)) -> ~ (forall x : T, A x).
Proof. 
  intros T A B C.
  intros H1 [a [HB HC]].
  contradict HC.
  assert (HAB: A a /\ B a). 
  { split. 
    - apply HC.
    - exact HB.
  }
  apply H1.
  - exists a.
    apply HAB.
  - exact HB.
Qed.

(* Inciso b *)
Theorem Ej4_IncB: 
  forall (T1 T2 : Type) (B G: T1 -> T2 -> Prop)
         (C W: T2 -> Prop) (m : T2),
  (exists x, (~ B x m /\ forall y, (C y -> ~G x y))) ->
  (forall z, (~ forall y, W y -> G z y) -> B z m) ->
  (forall x : T2, C x -> ~W x).
Proof.
  intros T1 T2 B G C W m.
  intros [x1 [H1 H2]] H3 x2 HC.
  apply H2 in HC.
  contradict H1.
  apply H3.
  contradict HC.
  apply HC in H1.
  exact H1.
Qed.

(* Inciso c *)
Theorem Ej4_IncC:
  forall (T1 T2 T3 T4: Type) 
         (P H C: T1 -> Prop)
         (L : T2 -> Prop )
         (A : T1 -> T2 -> Prop)
         (R : T3 -> Prop)
         (B : T3 -> T4 -> Prop),
  ( ~(forall x,(~P x \/ ~H x))
          -> forall x, (C x /\ forall y, (L y -> A x y))) ->
  ( (exists x, (H x /\ forall y, (L y -> A x y))) 
          ->  forall x, (R x /\ forall y, B x y)) ->
  ( ~(forall x y, B x y) -> (forall x, ~ P x \/ ~ H x)).
Proof.
  intros T1 T2 T3 T4 P H C L A R B.
  intros H1 H2 H3 x.
  apply not_all_ex_not in H3.
  destruct H3 as [x1 H3].
  apply not_all_ex_not in H3.
  destruct H3 as [y1 H3].
  rewrite DefImp in H2.
  destruct H2 as [H2 | H2].
  {
    rewrite DefImp in H1.
    rewrite edn in H1.
    destruct H1 as [H1|H1].
    { apply H1. }
    { right.
      assert(H4: forall y : T2, L y -> A x y). { apply H1. }
      assert(H5: forall x : T1, ~(H x /\ (forall y : T2, L y -> A x y))).
      { apply not_ex_all_not. apply H2. }
      contradict H5.
      apply ex_not_not_all.
      exists x.
      apply edn.
      split.
      - exact H5.
      - exact H4.
    }
  }
  { assert(H4: B x1 y1).
    { apply H2. }
    contradict H4.
    exact H3. 
  }
Qed.


