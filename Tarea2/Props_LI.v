
(* Tarea 2 - VF *)
(* Eduardo Pascual Aseff *)
(* Seccion de Lógica Intuicionista *)

(* Ejercicio 2 *)

Lemma Ej2IncA: forall (A B: Prop), ~~(A -> B) -> (~~A -> ~~B).
Proof.
  intros A B.
  unfold not.
  intros.
  assert (A1: (A -> B) -> False). { 
    intros.
    apply H0.
    intros.
    apply (H1 (H2 H3)). 
  }
  apply (H A1).
Qed.

Lemma Ej2IncB: forall (A B: Prop), (~~A -> ~~B) -> ~~(A -> B).
Proof.
  intros A B.
  unfold not.
  intros.
  apply H.
  { intros. 
    apply H0.
    intros HA.
    apply H1 in HA.
    destruct HA.
  }
  {
    intros HB.
    apply H0.
    intros HA.
    exact HB.
  }
Qed.

Theorem Ej2: forall (A B: Prop), ~~(A -> B) <-> (~~A -> ~~B).
Proof.
  intros A B.
  split.
  apply Ej2IncA.
  apply Ej2IncB.
Qed.