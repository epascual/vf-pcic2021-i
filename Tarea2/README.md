# Tarea 2 - Verificacion Formal

Nombre: Eduardo Pascual Aseff
Correo: eduardopascualaseff@gmail.com
Número de Cuenta: 520462566


## Distribución de los archivos
Los archivos están distribuidos por las secciones a resolver de la tarea, como se muestra a continuación.
Los archivos Examples.v y Props_LC.v dependen de algunas definiciones de Defs_LC.v

### Sección 1 - Lógica Minimal
Props_LM.v

### Sección 2 - Lógica Intuicionista
Props_LI.v

### Sección 3 - Lógica Clásica
Defs_LC.v
Props_LC.v

### Sección 4 - Cualquier sistema
EXAMPLES.v
