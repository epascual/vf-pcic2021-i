
(* Tarea 2 - VF *)
(* Eduardo Pascual Aseff *)
(* Seccion de Lógica Minimal *)

(* Ejercicio 1 *)
(* Inciso a, triple negacion es negacion *)
Theorem tripleNegIsNeg: forall A: Prop, ~~~A <-> ~A.
Proof.
  intros A.
  split.
  { 
    unfold not.
    intros H H0.
    apply H.
    intros.
    apply (H1 H0).
  }
  {
    unfold not.
    intros H H0.
    apply (H0 H).
  }
Qed.

(* Inciso b, la doble negacion se distribuye *)
Theorem doubleNegDist: forall (A B : Prop), ~~(A /\ B) -> ~~A /\ ~~B.
Proof.
  intros A B.
  intros H.
  unfold not in *. 
  split.
  { 
    intros H1.
    apply H.
    intros H2.
    destruct H2 as [HA HB].
    apply (H1 HA).
  }
  {
    intros H1.
    apply H.
    intros H2.
    destruct H2 as [HA HB].
    apply (H1 HB).
  }
Qed.

(* Inciso c, introduccion de doble negacion en para todo *)
Theorem doubleNegIntroInForAll: forall (X : Type) (A: X -> Prop),
  (~~ forall (x : X), A x) -> (forall (x : X), ~~ A x).
Proof.
  intros X A.
  unfold not in *.
  intros.
  apply H.
  intros.
  apply H0.
  apply H1.
Qed.


