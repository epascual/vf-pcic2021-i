
Definition cotenabilidad (A B : Prop) := ~(A->~B).

Notation "A ~> B" := (cotenabilidad A B)
  (at level 60) : type_scope.




(* Del script vfp211s03.v compartido por el profesor *)

Require Import Classical.
Require Export Classical_Prop.
Require Export Classical_Pred_Type.

Theorem edn (p : Prop): ~~p <-> p.
Proof.
  unfold iff.
  split.
  { exact (NNPP p). }
  { intros.
    contradict H.
    exact H.
    (* 
      (* Asi lo tenia el profe, pero bueno 
         lo cambie para no usar unfold not *)
    unfold not.
    intros.
    exact (H0 H). 
    *)
  }
Qed.


Theorem ContraPos (p q : Prop): (p -> q) <-> (~q -> ~p).
Proof.
  unfold iff.
  split.
  { intuition. }
  { intros.
    assert (q \/ ~q). {exact (classic q). }
    destruct H1.
    { exact H1. }
    { absurd p.
      - exact (H H1).
      - exact H0.
    }
  }
Qed.

Theorem DefImp (p q : Prop): (p -> q) <-> ~p \/ q.
Proof.
  unfold iff.
  split.
  { intro.
    assert (p\/~p). { exact (classic p). }
    destruct H0.
    { right;exact(H H0). }
    { left;assumption. }
  }
  {
    intros.
    destruct H.
    { absurd p;trivial;trivial. }
    { trivial. }
  }
Qed.


Lemma dMorganY: forall p q:Prop, ~(p/\q) <-> ~p \/ ~q.
Proof. 
  intros p q.
  split.
  - apply not_and_or. 
  - apply or_not_and.
Qed. 


Lemma dMorganO: forall p q:Prop, ~(p\/q) <-> ~p /\ ~q.
Proof. 
  intros p q.
  split. 
  - apply not_or_and.
  - apply and_not_or.  
Qed.
