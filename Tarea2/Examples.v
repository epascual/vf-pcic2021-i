Require Import Defs_LC.

(* Tarea 2 - VF *)
(* Eduardo Pascual Aseff *)
(* Seccion de Cualquier Sistema *)


(* Ejercicio 5 *)
(* Inciso a - Usando lógica clásica *)
Theorem Ej5_IncA: forall (A B C D : Prop),
  (A -> B) ->
  (~(A \/ C) -> D) ->
  (A \/ B -> C) ->
  (~D -> C).
Proof.
  intros A B C D.
  intros H1 H2 H3 H4.
  rewrite DefImp in *.
  rewrite edn in H2.
  rewrite dMorganO in H3.
  destruct H2 as [[HA|HC]|HD].
  { destruct H3 as [[HA2 HB2]| HC2].
    { contradict HA2. exact HA. }
    { exact HC2. }
  }
  { exact HC. }
  { contradict HD. exact H4. }
Qed.


(* Inciso b - Usando lógica clásica *)
Theorem Ej5_IncB: forall (A B C D : Prop),
  (A \/ B) -> 
  (~D -> C)/\(~B -> ~A)->(C->~B)-> D.
Proof.
  intros A B C D.
  intros HAB [H1 H2] H3.
  assert(D \/ ~D). exact (classic D).
  destruct H as [H|H].
  { exact H. }
  { apply H1 in H. 
    apply H3 in H.
    assert(HB: ~B). exact H.
    apply H2 in H.
    destruct HAB as [HA | HB2].
    { contradict H. exact HA. }
    { contradict HB. exact HB2. }
  }
Qed.


(* Inciso c - Usando lógica clásica *)
Theorem Ej5_IncC: forall (T : Type) (P B R : T -> Prop) (a : T),
  (forall x, P x -> ~ B x) ->
  R a -> (forall x, R x -> B x) -> ~ P a.
Proof.
  intros T P B R a.
  intros H1 H2 H3.
  apply H3 in H2.
  contradict H2.
  apply H1 in H2.
  exact H2.
Qed.


(* Inciso d - Usando lógica clásica *)
Theorem Ej5_IncD: forall (Ty : Type) (P B R S T : Ty -> Prop),
  (forall x, P x \/ B x -> ~ R x) ->
  (forall x, S x -> R x) ->
  forall x, P x -> ~ S x \/ T x.
Proof.
  intros Ty P B R S T.
  intros H1 H2 x1 HPx.
  assert (H3: ~ R x1). { apply H1. left. exact HPx. }
  left.
  contradict H3.
  apply H2 in H3.
  exact H3.
Qed.


(* Inciso e - Usando lógica clásica *)
(* Realmente esto no se cumple si T es un tipo "vacío" *)
Theorem Ej5_IncE: forall (T: Type) (P B : T -> Prop), 
  forall x : T, (* Esto se lo agregue *)
  (forall x, P x /\ exists y, B y) -> 
  exists x, P x /\ B x.
Proof.
  intros T P B x1.
  intros H1.
  assert (H2: P x1 /\ (exists y : T, B y) ).
  { apply H1. }
  destruct H2 as [_ [x2 H3]].
  exists x2.
  split.
  - apply H1.
  - apply H3.
Qed.


(* Inciso f - Usando lógica minimal *)
Theorem Ej5_IncF: 
  forall (T1 T2: Type) (P : T1 -> Prop) (R : T1 -> T2 -> Prop),
    (forall x, exists y, (P x -> R x y)) ->
    (forall x, P x -> exists y, R x y).
Proof.
  intros T1 T2 P R.
  intros H1 x1 H2.
  assert (H3: exists y : T2, P x1 -> R x1 y). {apply H1. }
  destruct H3 as [y1 H3].
  exists y1.
  apply H3.
  exact H2.
Qed.


(* Inciso g - Usando lógica clásica *)
Theorem Ej5_IncG:
  forall (T1 : Type) (P B : T1 -> Prop),
    (forall x, P x -> ~ B x) ->
    ~ exists x, P x /\ B x.
Proof.
  intros T1 P B H.
  assert(H1: forall x : T1, ~ P x \/ ~ B x).
  { intros x. rewrite <- DefImp. apply H. }
  apply all_not_not_ex.
  intros n.
  apply dMorganY.
  apply H1.
Qed.


(* Inciso h - Usando lógica minimal *)
Theorem Ej5_IncH:
  forall (T1 T2: Type) (P : T1 -> T2 -> Prop),
    (exists x, forall y, P x y) ->
    forall y, exists x, P x y.
Proof.
  intros T1 T2 P.
  intros [x1 H1] y1.
  exists x1.
  apply H1.
Qed.







