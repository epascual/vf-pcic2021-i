(* Definition of binary trees and some of their properties  *)
Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.
Require Import Lia.

Parameter (A:Type)
          (eq_dec_A: forall (x y:A),{x=y}+{x<>y})   (* x=y \/ x<>y *)
          (undefA : A).          

(* Binary trees*)
Inductive BTree : Type :=
    E : BTree   
  | N : A -> BTree  -> BTree  -> BTree.

Parameter (undefBTree : BTree).          

Create HintDb FlexArrayDb.

Theorem eq_btree_dec: forall (s t:BTree), {s=t} + {s<>t}.
Proof.
intros.
decide equality.
apply eq_dec_A.
Qed.

Hint Resolve eq_btree_dec:FlexArrayDB.

Lemma btree_nonE: forall (t:BTree), 
  t <> E -> exists (a:A) (t1 t2:BTree), t = N a t1 t2.
Proof.
intros.
destruct t.
intuition.
exists a.
exists t1.
exists t2.
trivial.
Qed.

Hint Resolve btree_nonE:FlexArrayDB.

Fixpoint size (t:BTree) : nat :=
 match t with
  |E => 0
  |N x s t => S(size s + size t)
 end.



(* Every Braun tree most be a balanced tree according to the next definition *)
Inductive bal : BTree -> Prop :=
  |balE  : bal E
  |balN : forall (a:A) (s t: BTree), bal s -> bal t -> 
                      size t <= size s -> size s <= S(size t) -> bal (N a s t).


(* Definition of the lookup function *)
Fixpoint lookup (t:BTree) (i: nat) : A :=
 match t,i with
  |E,i => undefA
  |N x s t,0 => x
  |N x s t,i => if eqb (modulo i 2) 1 then lookup s (div i 2) else lookup t (div i 2-1)
 end.

Fixpoint upd (t:BTree) (i: nat) (x : A) : BTree :=
 match t,i with
  |E,i => undefBTree 
  |N y s t, 0 =>  N x s t
  |N y s t,i => if eqb (modulo i 2) 1 then N y (upd s (div i 2) x) t  
                       else N y s (upd t (div i 2-1) x)
 end.
 
 Check Nat.eq_dec.

Theorem size0_E: forall (t1 : BTree),
  size t1 = 0 -> t1 = E.
Proof.
  intros t1 H.
  destruct t1.
  - reflexivity.
  - inversion H.
Qed.

Theorem rightE_nat
     : forall (a : A) (t1 t2 : BTree ),
       bal (N a t1 t2) ->
       t2 = E -> t1 = E \/ (exists aa : A, t1 = N aa E E).
Proof.
  intros a t1 t2 Hbal Ht2.
  rewrite Ht2 in Hbal.
  inversion Hbal.
  destruct t1.
  { left. reflexivity. }
  { right.
    simpl in H5.
    assert(t1_1 = E). 
    { apply size0_E. lia. }
    assert(t1_2 = E). 
    { apply size0_E. lia. }
    rewrite H6.
    rewrite H7.
    exists a1.
    trivial.
  } 
Qed.














