Load lookupdBN.

Fixpoint le (x:A) (t:BTree) : BTree :=
 match t with
    E => N x E E
  | N y s t => N x (le y t) s
 end.

Fixpoint he (x:A) (t:BTree) : BTree  :=
 match t with
  |E => N x E E
  |N y l r => match bsize t with
               |U b => N y (he x l) r
               |D b => N y l (he x r)
               |  Z => undefBTree 
              end
 end.

(*
Lemma le_nonE: forall (a x:A) (t1 t2:BTree), le x (N a t1 t2) =  N x (le a t2) t1.
*)


Lemma bsize_le: forall (t:BTree) (x:A), 
                bsize (le x t) = sucBN (bsize t).
Proof.
intro.
assert (HBal := allBal t).  
induction HBal.
- reflexivity.
- intro.
  simpl.
  rewrite IHHBal2.
  rewrite <- plusSuc.
  rewrite plusComm.
  reflexivity.
Qed.



Lemma bal_le: forall (t:BTree), bbal t -> 
                 forall (x:A), bbal (le x t).
Proof.
intros t HtBal.
induction HtBal.
- simpl.
  apply bbal_leaf.
- intros.
  simpl.
  constructor.
  + apply IHHtBal2.
  + assumption.
  + rewrite bsize_le.
    assumption.
  + rewrite bsize_le.
    apply lteqBN_suc.
    assumption.
Qed.

Lemma le_head: forall (t: BTree) (x:A),  lookup_bn (le x t) Z = x.
Proof.
intros.
destruct t.
- intuition.
- intuition.
Qed.


Lemma le_idx: forall (t:BTree),  bbal t -> 
              forall (j:BN), j <BN (bsize t) -> 
              forall (x:A), 
              lookup_bn (le x t) (sucBN j) = lookup_bn t j.
Proof.
intros t B.
induction B.
- intros.
  simpl in H.
  inversion H.
- intros.
  clear IHB1.
  destruct j.
  + simpl.
    apply le_head.
  + reflexivity.
  + simpl.
    apply IHB2.
    apply (lt_D_bsize j a s t).
    assumption.
Qed.


(*High Extension*)

Lemma bsize_he: forall (t:BTree) (x:A), 
                    bsize (he x t) = sucBN (bsize t).
Proof.
intro.
induction t.
- intuition.
- intros.
  destruct (bbal_size_r a t1 t2).
  + simpl in H.
    simpl.     
    rewrite H.
    simpl.
    rewrite IHt1.
    rewrite <- plusSuc.
    rewrite H. 
    intuition.
  + simpl in H.
    simpl.
    rewrite H.
    simpl.
    rewrite IHt2.
    rewrite <- plusSuc_2.
    rewrite H.
    intuition.
Qed.



Lemma bal_he: forall (t:BTree), bbal t -> 
                forall (x:A), bbal (he x t).
Proof.
intros t Ht.
induction t.
- simpl.
  apply bbal_leaf.
- intros.
  inversion Ht.
  subst.
  destruct (bbal_size_r a t1 t2).
  + assert(H6:=H).
    apply size_caseU in H.
    simpl in H6.
    simpl.
    rewrite H6.
    constructor.
    * apply IHt1.
      assumption.
    * assumption.
    * rewrite bsize_he.
      inversion H4.
      -- intuition.
      -- admit.
    * rewrite bsize_he.
      rewrite H.
      intuition.
  + assert(H6:=H).
    apply size_caseD in H.
    simpl in H6.
    simpl.
    rewrite H6.
    constructor.
Admitted.       
       





Lemma he_last: forall (t: BTree) (x:A),  
               lookup_bn (he x t) (bsize t) = x.
Admitted.
(* Tarea moral *)



Lemma he_idx: forall (t:BTree),  bbal t -> 
              forall (j:BN), j <BN (bsize t) -> 
              forall (x:A), lookup_bn (he x t) j = lookup_bn t j.
Proof. 
induction t.
- intros.
  inversion H0.
- intros.
  assert(bsize (N a t1 t2) = sucBN(bsize t1 ⊞ bsize t2)).
  { reflexivity. }
  destruct (bsize (N a t1 t2)).
  + contradict H1. 
    apply ZnotSucBN.
  + simpl.
    rewrite <- H1.
    destruct j.
    * reflexivity.
    * simpl.
      apply IHt1.
      ** inversion H; assumption.
      ** admit.  
    * reflexivity.
  + simpl.
    rewrite <- H1.
    destruct j.
    * reflexivity.
    * reflexivity.
    * apply IHt2.
      ** inversion H; assumption.
      ** admit.
Admitted.










(* Tarea moral *)


