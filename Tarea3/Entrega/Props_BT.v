(* 
   Propiedades de los arreglos extensibles 
      definiods a partir de árboles de Braun
*)


(********************************************
 * Sección copiada de los scripts de clase! *
 ********************************************)

Load Defs_BT.

Theorem eq_btree_dec: forall (s t:BTree), {s=t} + {s<>t}.
Proof.
intros.
decide equality.
apply eq_dec_A.
Qed.


Lemma nonE_tree: forall (t:BTree), t <> E -> exists (a:A) (t1 t2:BTree), t = N a t1 t2.
Proof.
intros.
destruct t.
intuition.
exists a.
exists t1.
exists t2.
trivial.
Qed.





Lemma bsize_Z: forall (t:BTree), bsize t = Z -> t = E.
Proof.
intros t0.
destruct t0.
intuition.
intros.
simpl in H.
symmetry in H.
contradict H.
apply ZnotSucBN.
Qed.

Lemma bsize_nonZ: forall (t:BTree), t <> E -> bsize t <> Z.
Proof.
intros.
contradict H.
apply bsize_Z.
trivial.
Qed.


Lemma btNonE: forall (t:BTree) (b:BN), t <> E -> 
                       exists (b:BN), bsize t = U b \/ bsize t = D b.
Proof.
intros.
apply bsize_nonZ in H.
apply (bnNonZ (bsize t)) in H.
trivial.
Qed.







Lemma prop_0_U : forall (a:A) (s t:BTree) (b:BN), 
                  bbal (N a s t) -> bsize(N a s t) = U b -> 
                  bsize s = b /\ bsize t = b.
Proof.
intros.
simpl in H0.
assert (H0b:=H0).
rewrite <- plus_U in H0.
apply SucBNinj in H0.
inversion H.
destruct(bbalCond_eqs (bsize s) (bsize t)).
trivial.
trivial.
rewrite <- H8 in H0.
apply addition_a_a in H0.
rewrite <- H8.
intuition.
rewrite H8 in H0b.
rewrite plus_D in H0b.
inversion H0b.
Qed.


Lemma prop_0_D : forall (a:A) (s t:BTree) (b:BN), bbal (N a s t) 
                         -> bsize(N a s t) = D b -> 
                            bsize s = sucBN b /\ bsize t = b.
Proof.
intros.
simpl in H0.
assert (H0b:=H0).
rewrite <- plus_D in H0.
apply SucBNinj in H0.
inversion H.
destruct(bbalCond_eqs (bsize s) (bsize t)).
trivial.
trivial.
rewrite <- H8 in H0b.
rewrite plus_U in H0b.
inversion H0b.
rewrite H8 in H0.
apply addition_SucBNa_a in H0.
rewrite <- H0.
intuition.
Qed.

Corollary size_caseU: forall (a:A) (l r:BTree) (b:BN), 
                        bsize (N a l r) = U b -> bsize l = bsize r.
Proof.
intros.
assert (HBal := allBal (N a l r)).
apply (prop_0_U a l r b) in H.
intuition.
rewrite <- H1 in H0.
intuition. intuition.
Qed.

Corollary size_caseD: forall (a:A) (l r:BTree) (b:BN), 
                        bsize (N a l r) = D b 
                           -> bsize l = sucBN (bsize r).
Proof.
intros.
assert (HBal := allBal (N a l r)).
apply (prop_0_D a l r b) in H.
intuition.
rewrite <- H1 in H0.
intuition. intuition.
Qed.

Corollary bbal_size_r: forall (a:A) (l r:BTree), 
                          bsize (N a l r) = U (bsize r) \/ 
                          bsize (N a l r) = D (bsize r).
Proof.
intros.
assert (HBal:=allBal (N a l r)).
destruct (bnNonZ (bsize (N a l r))).
simpl.
assert (Z <> sucBN (bsize l ⊞ bsize r)).
apply ZnotSucBN.
intuition.
destruct H.
apply prop_0_U in H.
simpl.
destruct H.
rewrite H.
rewrite H0.
rewrite plus_U.
intuition.
trivial.
apply prop_0_D in H.
destruct H.
simpl.
rewrite H.
rewrite H0.
rewrite plus_D.
intuition.
trivial.
Qed.

Theorem bbal_size_r2: forall (a:A) (l r:BTree), 
              (bsize (N a l r)) ≤BN (D (bsize r)). 
Proof.
intros a l r.
destruct (bbal_size_r a l r).
constructor.
rewrite H.
constructor.
rewrite H.
constructor.
Qed.

Theorem bbal_size_l: 
       forall (a:A) (l r:BTree), (bsize (N a l r)) ≤BN (U (bsize l)). 
Proof.
intros.
assert (HBal:=allBal (N a l r)).
destruct (bnNonZ (bsize (N a l r))).
- simpl.
  assert (Z <> sucBN (bsize l ⊞ bsize r)).
  apply ZnotSucBN.
  intuition.
- destruct H.
  + apply prop_0_U in H.
    * simpl.
      destruct H.
      subst.
      rewrite H0. 
      rewrite plus_U.
      constructor.
    * assumption.
  +  apply prop_0_D in H.
    * simpl.
      destruct H.
rewrite H.
rewrite H0.
rewrite plus_D.
constructor.
constructor.
apply lts.
* trivial.
Qed.

(* ============================================= *)


Lemma lt_U_bsize: forall (b:BN) (a:A) (t1 t2:BTree), 
               (U b) <BN (bsize (N a t1 t2)) -> b <BN (bsize t1).
Proof.
intros b a t1 t2 H.
assert ((bsize (N a t1 t2)) ≤BN (U (bsize t1))).
apply bbal_size_l.
assert ((U b) <BN (U (bsize t1))).
eapply lt_lteqBN_trans.
eexact H.
trivial.
inversion H1.
trivial.
Qed.

Theorem rightE: forall (a:A) (t1 t2:BTree), 
bbal(N a t1 t2) -> t2 = E -> (t1 = E \/ exists (aa:A), t1 = (N aa E E)).
Proof.
intros.
inversion H.
destruct (bbalCond_eqs (bsize t1) (bsize t2)).
trivial.
trivial.
rewrite H0 in H8.
simpl in H8.
apply bsize_Z in H8.
intuition.
rewrite H0 in H8.
right.
destruct t1.
simpl in H8.
inversion H8.
simpl in H8.
replace (U Z) with (sucBN Z) in H8.
apply SucBNinj in H8.
apply plusBN_Z_Z in H8.
destruct H8.
apply bsize_Z in H8.
apply bsize_Z in H9.
exists a1.
rewrite H8.
rewrite H9.
trivial.
intuition.
Qed.

Lemma lt_D_bsize: forall (b:BN) (a:A) (t1 t2:BTree), 
  (D b) <BN (bsize (N a t1 t2)) -> b <BN (bsize t2).
Proof.
intros b a t1 t2 H.
assert ((bsize (N a t1 t2)) ≤BN (D (bsize t2))).
apply bbal_size_r2.
assert ((D b) <BN (D (bsize t2))).
eapply lt_lteqBN_trans.
eexact H.
trivial.
inversion H1.
trivial.
Qed.

Lemma bbal_leaf: forall (a:A), bbal (N a E E).
Proof.
intro a.
constructor.
constructor.
constructor.
apply lteqBN_refl. 
apply lteqs.
Qed.


Theorem leftE_leaf: forall (t1 t2:BTree) (a:A), 
          bbal (N a t1 t2) -> t1 = E -> t2 = E.
Proof.
intros t1 t2 c HBal H.
inversion HBal.
rewrite H in H5.
simpl in H5.
inversion H5.
apply bsize_Z in H9.
trivial.
inversion H7.
Qed.


Lemma bbal_inv: forall (t:BTree), t <> E ->  
        (exists (z:A), t = N z E E)  \/ 
         exists (z:A) (r1 r2:BTree), 
                      bbal r1 /\ bbal r2 /\ r1 <> E /\ t = N z r1 r2.
Proof.
Admitted.













Lemma lkp_upd_BN: forall (t:BTree) (x:A) (b:BN), t <> E -> 
                       b <BN (bsize t) -> 
                       lookup_bn (update t b x) b = x.
Proof.
intros t x.
assert (H:=allBal t).
(*Induction on t*)
induction t.
- (*Base case t = E *)
intuition.
- (*Induction step t = N a t1 t2*)
intros.
(*cases on BNat number b*)
destruct b.
+ (*1. b=Z*)
reflexivity.
+ (*2. b = U b*)
destruct (eq_btree_dec t1 E).
(*Cases on t1*)
* (*i) t1=E A*)
assert (t2 = E).
-- apply (leftE_leaf t1 t2 a).
   ++ eexact H.
   ++ assumption.
-- (*t1=E  and t2=E *)
   subst.
   simpl in H1.
   inversion H1.
   inversion H4.
* (*ii) t1<>E A*)
simpl. 
apply IHt1.
-- inversion H.
   assumption.
-- assumption.
-- eapply lt_U_bsize.
   exact H1.
+ (*3. b = D b*)
  destruct (eq_btree_dec t2 E).
  * destruct (rightE a t1 t2).
    -- assumption.
    -- assumption.
    -- simpl.
       subst.
       simpl in H1.
       inversion H1.
       inversion H4.
    -- destruct H2.
       subst.
       simpl in H1.
       inversion H1.
       inversion H4.
* simpl. 
  apply IHt2.
  -- inversion H.
     assumption.
  -- assumption.
  -- eapply lt_D_bsize.
     exact H1.
Qed.




Lemma lkp_upd_BNindbal: forall (t:BTree) (x:A) (b:BN), t <> E -> 
                       b <BN (bsize t) -> 
                       lookup_bn (update t b x) b = x.
Proof.
intros t x.
assert (H:=allBal t).
induction H.
- intuition.
- intros.
  destruct b.
  + reflexivity.
  + simpl.
    destruct (eq_btree_dec s E).
    * destruct (eq_btree_dec t E).
      -- subst.
         simpl in H4.
         apply lt_U in H4.
         inversion H4.
      -- subst.
         simpl in H1.
         inversion H1. 
         ++ subst.
            apply bsize_nonZ in n.
            contradiction n.  
         ++ inversion H5.
    * apply IHbbal1.
      -- assumption.
      -- apply lt_U.
         eapply lt_lteqBN_trans.
         ++ exact H4.
         ++ apply bbal_size_l.
  + destruct (eq_btree_dec t E).
    * destruct (eq_btree_dec s E). 
       -- subst.
          simpl in H4.
          inversion H4.
          inversion H7.
       -- subst.
          simpl in H2.
          inversion H2.
          ++ simpl in H4.
             rewrite H7 in H4.
             simpl in H4. 
             inversion H4.
             inversion H9.
          ++ subst.
             inversion H5.
             ** contradict n.
             apply bsize_Z.
             intuition. 
             ** inversion H8.
             ** inversion H8.
    *  simpl.
       apply IHbbal2.
       -- assumption.
       -- apply lt_D.
          eapply lt_lteqBN_trans.
          ++ exact H4.
          ++ apply bbal_size_r2.  
Qed.


Lemma elmnt_lkp_upd : forall (t:BTree) (i j:BN), 
                        i <BN (bsize t) -> j <BN (bsize t) -> 
                        i <> j -> 
                        forall (x:A), 
                          lookup_bn (update t i x) j = lookup_bn t j.
Proof.
intros t.
induction t.
(* t = E*)
- intros.
simpl in H0.
inversion H0.
- (* t = N a t1 t2 *)
intros.
assert (tBal:=allBal (N a t1 t2)).
destruct (bbal_inv (N a t1 t2)).
+ discriminate.
+ (* exists z : A, N a t1 t2 = N z E E *)
destruct H2.
inversion H2.
subst.
simpl in H.
inversion H.
* subst.
  simpl in H0.
  inversion H0.
  -- subst. intuition.
  -- reflexivity.
  -- reflexivity. 
* destruct j.
  -- reflexivity.
  -- inversion H5.
  -- inversion H5.
* inversion H5.
+ (*  exists (z : A) (r1 r2 : BTree),
         bbal r1 /\ bbal r2 /\ r1 <> E /\ N a t1 t2 = N z r1 r2 *)
do 4 (destruct H2).
destruct H3.
destruct H4.
destruct H5.
destruct i.
* destruct j. 
  -- intuition.
  -- reflexivity.
  -- reflexivity.
* destruct j.
  -- reflexivity.
  -- simpl.
     apply IHt1. 
     ++ apply lt_U.
        eapply lt_lteqBN_trans.
        ** exact H.
        ** apply bbal_size_l. 
     ++ apply lt_U.
        eapply lt_lteqBN_trans.
        ** exact H0.
        ** apply bbal_size_l.
     ++ contradict H1.
        subst;reflexivity.
   -- reflexivity.
  * destruct j.
    -- reflexivity.
    -- reflexivity.
    -- simpl. 
       apply IHt2. 
     ++ apply lt_D.
        eapply lt_lteqBN_trans.
        ** exact H.
        ** apply bbal_size_r2.
     ++ apply lt_D.
        eapply lt_lteqBN_trans.
        ** exact H0.
        ** apply bbal_size_r2.
     ++ contradict H1.
        subst;reflexivity.
Qed.


Lemma bsize_upd: forall (t:BTree) (x:A) (b:BN), 
                  b <BN bsize t -> bsize t = bsize (update t b x).
Proof.
intro t.
induction t.
- (* Base case *)
intuition.
inversion H.
- (* Inductive step *)
intros.
destruct (bbal_inv (N a t1 t2)).
+ discriminate.
+ destruct H0.
  rewrite H0 in H.
  simpl in H.
  inversion H.
  * (* b = Z *)
   reflexivity.
  * (* U a0 = b, a < Z *)
    inversion H3.
  * (* D a0 = b, a < Z *)
    inversion H3.
+ do 4 (destruct H0).
  destruct H1.
  destruct H2.
  inversion H3.
  subst.
  destruct b.
  * (* Z *)
    reflexivity.
  * (* U b*)
   simpl.
   rewrite (IHt1 x b).
   -- reflexivity.
   -- apply (lt_U_bsize b x0 x1 x2).
      assumption. 
  * (* b = D b *)
    simpl.
    rewrite (IHt2 x b).
    -- reflexivity.
    -- apply (lt_D_bsize b x0 x1 x2).
       assumption.
Qed.
     
  









Lemma bsize_le: forall (t:BTree) (x:A), 
                bsize (le x t) = sucBN (bsize t).
Proof.
intro.
assert (HBal := allBal t).  
induction HBal.
- reflexivity.
- intro.
  simpl.
  rewrite IHHBal2.
  rewrite <- plusSuc.
  rewrite plusComm.
  reflexivity.
Qed.



Lemma bal_le: forall (t:BTree), bbal t -> 
                 forall (x:A), bbal (le x t).
Proof.
intros t HtBal.
induction HtBal.
- simpl.
  apply bbal_leaf.
- intros.
  simpl.
  constructor.
  + apply IHHtBal2.
  + assumption.
  + rewrite bsize_le.
    assumption.
  + rewrite bsize_le.
    apply lteqBN_suc.
    assumption.
Qed.

Lemma le_head: forall (t: BTree) (x:A),  lookup_bn (le x t) Z = x.
Proof.
intros.
destruct t.
- intuition.
- intuition.
Qed.


Lemma le_idx: forall (t:BTree),  bbal t -> 
              forall (j:BN), j <BN (bsize t) -> 
              forall (x:A), 
              lookup_bn (le x t) (sucBN j) = lookup_bn t j.
Proof.
intros t B.
induction B.
- intros.
  simpl in H.
  inversion H.
- intros.
  clear IHB1.
  destruct j.
  + simpl.
    apply le_head.
  + reflexivity.
  + simpl.
    apply IHB2.
    apply (lt_D_bsize j a s t).
    assumption.
Qed.


(*High Extension*)

Lemma bsize_he: forall (t:BTree) (x:A), 
                    bsize (he x t) = sucBN (bsize t).
Proof.
intro.
induction t.
- intuition.
- intros.
  destruct (bbal_size_r a t1 t2).
  + simpl in H.
    simpl.     
    rewrite H.
    simpl.
    rewrite IHt1.
    rewrite <- plusSuc.
    rewrite H. 
    intuition.
  + simpl in H.
    simpl.
    rewrite H.
    simpl.
    rewrite IHt2.
    rewrite <- plusSuc_2.
    rewrite H.
    intuition.
Qed.



Lemma bal_he: forall (t:BTree), bbal t -> 
                forall (x:A), bbal (he x t).
Proof.
intros t Ht.
induction t.
- simpl.
  apply bbal_leaf.
- intros.
  inversion Ht.
  subst.
  destruct (bbal_size_r a t1 t2).
  + assert(H6:=H).
    apply size_caseU in H.
    simpl in H6.
    simpl.
    rewrite H6.
    constructor.
    * apply IHt1.
      assumption.
    * assumption.
    * rewrite bsize_he.
      inversion H4.
      -- intuition.
      -- admit.
    * rewrite bsize_he.
      rewrite H.
      intuition.
  + assert(H6:=H).
    apply size_caseD in H.
    simpl in H6.
    simpl.
    rewrite H6.
    constructor.
Admitted.       
       





Lemma he_last: forall (t: BTree) (x:A),  
               lookup_bn (he x t) (bsize t) = x.
Admitted.
(* Tarea moral *)



Lemma he_idx: forall (t:BTree),  bbal t -> 
              forall (j:BN), j <BN (bsize t) -> 
              forall (x:A), lookup_bn (he x t) j = lookup_bn t j.
Proof. 
induction t.
- intros.
  inversion H0.
- intros.
  assert(bsize (N a t1 t2) = sucBN(bsize t1 ⊞ bsize t2)).
  { reflexivity. }
  destruct (bsize (N a t1 t2)).
  + contradict H1. 
    apply ZnotSucBN.
  + simpl.
    rewrite <- H1.
    destruct j.
    * reflexivity.
    * simpl.
      apply IHt1.
      ** inversion H; assumption.
      ** admit.  
    * reflexivity.
  + simpl.
    rewrite <- H1.
    destruct j.
    * reflexivity.
    * reflexivity.
    * apply IHt2.
      ** inversion H; assumption.
      ** admit.
Admitted.










(**************************************************
 **************************************************
 **       Propiedades agregadas por mi           **
 **************************************************
 **************************************************)

Lemma le_not_E: forall (t: BTree) (x:A),
  le x t <> E.
Proof.
intros.
destruct t.
- intros H. 
  inversion H.
- intros H.
  inversion H.
Qed.

Lemma le_is_N: forall (t: BTree) (x:A),
  exists (t1 t2: BTree), le x t = N x t1 t2.
Proof.
intros.
destruct t.
- exists E.
  exists E.
  reflexivity.
- exists (le a t2).
  exists t1.
  reflexivity.
Qed.





(****************************************************
 ****************************************************
 **       Tarea: Propiedades de las operaciones    **
 **              de remoción.                      **
 ****************************************************
 ****************************************************)


(* 
  2- bsize (lr t) = pred(bsize t) 
     Precondiciones: + t tiene un elemento
                     + t esta balanceado
*)
Lemma bsize_lr: forall (t:BTree), Z <BN bsize t /\ bbal t -> 
                bsize (lr t) = predBN (bsize t).
Proof.
intros t1 [Hsize HBal].
induction HBal.
- inversion Hsize. 
- destruct s.
  + simpl in H.
    assert(t = E).
    { apply bsize_Z.  
      inversion H. 
      - reflexivity.
      - inversion H1.
    }
    rewrite H1.
    reflexivity.
  + assert( lr (N a (N a0 s1 s2) t) = N a0 t (lr (N a0 s1 s2))).
    { reflexivity. }
    rewrite H1.
    assert(bsize (N a0 t (lr (N a0 s1 s2))) = 
           sucBN (bsize t ⊞ bsize (lr (N a0 s1 s2)) )).
    { reflexivity. }
    rewrite H2.
    rewrite IHHBal1.
    * simpl. 
      rewrite predsucBNinv.
      rewrite predsucBNinv.
      rewrite plusComm.
      rewrite plusSuc. 
      reflexivity.
    * simpl.
      apply ltBN_Z.
Qed.


(* 
  3- bbal t -> bbal (lr t)
     Precondiciones: bbal undefBTree, sale por allBal
*)
Lemma bal_lr: forall (t:BTree), bbal t -> bbal (lr t).
Proof.
intros t Hbal.
induction t.
- simpl.
  apply allBal.
- inversion Hbal.
  specialize (IHt1 H2). 
  specialize (IHt2 H3).
  destruct t1.
  + simpl. 
    assumption.
  + assert(A1: lr (N a (N a1 t1_1 t1_2) t2) = N a1 t2 (lr (N a1 t1_1 t1_2))).
    { reflexivity. }
    rewrite A1.
    constructor.
    * assumption.
    * apply IHt1.
    * rewrite bsize_lr.
      { apply lteqBN_suc_pred. 
        - simpl. 
          intros HH. symmetry in HH. contradict HH.
          apply ZnotSucBN.
        - assumption.
      }
      { split. 
        { simpl. apply ltBN_Z. }
        { assumption. }
      }
    * rewrite bsize_lr.
      { rewrite sucpredBNinv.
        - assumption.
        - intros HH. symmetry in HH. contradict HH.
          apply ZnotSucBN. 
      }
      { split. 
        { simpl. apply ltBN_Z. }
        { assumption. }
      }
Qed.


(* 
  4- lookup_bn (lr t) j = lookup_bn t (sucBN j)
     Precondiciones: + bbal t
                     + sucBN j < bsize t
*)
Lemma lr_idx: forall (t: BTree) (j: BN),
  bbal t -> sucBN j <BN bsize t ->
  lookup_bn (lr t) j = lookup_bn t (sucBN j).
Proof.
intros t.
induction t.
{ intros j _ Hsize. inversion Hsize. }
{ intros j Hbal Hsize.
  inversion Hbal.
  destruct j.
  { destruct t1; reflexivity. }
  { destruct t1. 
    { assert(A1: t2 = E).
      { apply bsize_Z. 
        inversion H4.
        - reflexivity.
        - inversion H6.
      }
      rewrite A1.
      reflexivity.
    }
    { reflexivity. }
  }
  {
    destruct t1.
    { reflexivity. }
    { assert(A1: lr (N a (N a1 t1_1 t1_2) t2) = 
                 N a1 t2 (lr (N a1 t1_1 t1_2))).
      { reflexivity. }
      rewrite A1. clear A1.
      assert(A2: lookup_bn (N a1 t2 (lr (N a1 t1_1 t1_2))) (D j) =
                 lookup_bn (lr (N a1 t1_1 t1_2)) j). 
      { reflexivity. }
      rewrite A2. clear A2.
      rewrite IHt1. 
      - reflexivity.
      - assumption.
      - clear IHt1. 
        clear IHt2. 
        assert(A1: bsize (N a (N a1 t1_1 t1_2) t2) =
                   sucBN (bsize (N a1 t1_1 t1_2) ⊞ bsize t2) ).
        { reflexivity. }
        rewrite A1 in Hsize. clear A1.
        assert(A1: D j <BN bsize (N a1 t1_1 t1_2) ⊞ bsize t2).
        { apply ltBN_suci. assumption. }
        assert(A3:  bsize (N a1 t1_1 t1_2) = bsize t2
                 \/  bsize (N a1 t1_1 t1_2) = sucBN (bsize t2)).
        { apply (bbalCond_eqs _ _ H4 H5). }
        destruct A3 as [A3 | A3].
        + rewrite <- A3 in A1.
          assert(A4: bsize (N a1 t1_1 t1_2) ⊞ bsize (N a1 t1_1 t1_2)
                   = D (predBN (bsize (N a1 t1_1 t1_2)))).
          { simpl. 
            rewrite predsucBNinv. 
            rewrite <- plus_D. 
            rewrite <- plusSuc_2. 
            reflexivity.
          }
          rewrite A4 in A1. clear A4.
          apply <- lt_D in A1.
          apply ltBN_suc in A1.
          rewrite sucpredBNinv in A1.
          * assumption.
          * simpl. 
            intros HH. symmetry in HH. contradict HH.
            apply ZnotSucBN.
        + rewrite A3 in A1.
          rewrite <- plusSuc in A1.
          rewrite plus_U in A1.
          inversion A1.
          apply ltBN_suc in H8.
          rewrite A3.
          assumption.
    }
  }
}
Qed.


(* 
  5 - lr (le x t) = t.
*)
Lemma lr_le_inv: forall (t: BTree) (x:A),  lr (le x t) = t.
Proof.
intros t.
induction t.
- reflexivity.
- intros. 
  assert(A1: le x (N a t1 t2) = N x (le a t2) t1). 
  { reflexivity. }
  rewrite A1. 
  assert(A2 := le_is_N t2 a).
  destruct A2 as [t3 [t4 A2]].
  simpl.
  rewrite IHt2.
  rewrite A2.
  reflexivity.
Qed.


(* 
  6 - le (lookup t Z) (lr t) = t
      Precondicion: t tiene un elemento
                    bbal t
*)
Lemma le_lr_inv: forall (t:BTree), Z <BN bsize t -> bbal t ->
                 le (lookup_bn t Z) (lr t) = t.
Proof.
intros t.
induction t.
- intros Hsize. inversion Hsize.
- intros Hsize Hbal.
  assert(A1:= bn_is0_or_gt0 (bsize t1)).
  destruct A1 as [A1 | A1].
  { assert(Ht1: t1 = E).
    { apply bsize_Z. rewrite A1. reflexivity. }
    assert(Ht2: t2 = E).
    { inversion Hbal.
      apply bsize_Z.
      rewrite <- A1 in H4.
      inversion H4.
      - reflexivity.
      - inversion H6.
    }
    rewrite Ht1.
    rewrite Ht2.
    reflexivity.
  }
  { inversion Hbal.
    specialize (IHt1 A1 H2).
    assert(A2: lookup_bn (N a t1 t2) Z = a).
    { reflexivity. }
    rewrite A2. clear A2.
    simpl.
    destruct t1.
    - inversion A1.
    - assert(A2: le a (N a1 t2 (lr (N a1 t1_1 t1_2)))
               = N a (le a1 (lr (N a1 t1_1 t1_2))) t2).
      { reflexivity. } 
      rewrite A2. clear A2.
      assert(A2: lookup_bn (N a1 t1_1 t1_2) Z = a1).
      { reflexivity. }
      rewrite A2 in IHt1.
      rewrite IHt1.
      reflexivity.
  }
Qed.

(* 
  7- bsize (hr t) = pred(bsize t) 
     Precondiciones: + t tiene un elemento
                     + t esta balanceado
*)
Lemma bsize_hr: forall (t:BTree), Z <BN bsize t /\ bbal t -> 
                bsize (hr t) = predBN (bsize t).
Proof.
intros t.
induction t.
- intros [H _].
  inversion H.
- intros [Hsize Hbal].
  destruct t1.
  + assert(H2: t2 = E). 
    { apply bsize_Z. 
      inversion Hbal.
      inversion H4.
      - reflexivity.
      - inversion H6.
    }
    rewrite H2.
    reflexivity.
  + assert(IH1_dep: Z <BN bsize (N a0 t1_1 t1_2) /\ bbal (N a0 t1_1 t1_2)).
    { split.
      - simpl.
        apply ltBN_Z.
      - inversion Hbal.
        assumption.
    }
    specialize (IHt1 IH1_dep). clear IH1_dep.
    destruct t2.
    ++ assert(A1: bsize t1_1 ⊞ bsize t1_2 = Z).
       { inversion Hbal. 
         simpl in H5.
         assert(U Z = sucBN Z). { reflexivity. }
         rewrite H6 in H5.
         apply lteqBN_suci in H5.
         inversion H5.
         * reflexivity.
         * inversion H7.
       }
       assert(A2: t1_1 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       assert(A3: t1_2 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       rewrite A2. rewrite A3.
       reflexivity.
    ++ assert(IH2_dep: Z <BN bsize (N a1 t2_1 t2_2) /\ bbal (N a1 t2_1 t2_2)).
       { split.
         - simpl.
           apply ltBN_Z.
         - inversion Hbal.
           assumption.
       } 
       specialize (IHt2 IH2_dep). clear IH2_dep.
       assert(A1:  predBN (bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                 = bsize (N a0 t1_1 t1_2) ⊞ bsize (N a1 t2_1 t2_2)).
       { simpl. rewrite predsucBNinv. reflexivity. }
       rewrite A1. clear A1.
       destruct (bbal_size_r a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)).
       * assert(A2:  bsize (hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                   = sucBN (bsize (N a0 t1_1 t1_2) ⊞ bsize (hr (N a1 t2_1 t2_2)))).
         { simpl in H.
           simpl. 
           rewrite H. 
           reflexivity.
         }
         rewrite A2.
         rewrite IHt2. 
         rewrite plusSuc_2.
         rewrite sucpredBNinv.
         ** reflexivity.
         ** simpl. 
            intros HH.
            symmetry in HH.
            contradict HH.
            apply ZnotSucBN.
       * assert(A2:  bsize (hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                   = sucBN (bsize (hr (N a0 t1_1 t1_2)) ⊞ bsize (N a1 t2_1 t2_2))).
         { simpl in H.
           simpl. 
           rewrite H. 
           reflexivity.
         }
         rewrite A2.
         rewrite IHt1.
         rewrite plusSuc.
         rewrite sucpredBNinv.
         ** reflexivity.
         ** simpl. 
            intros HH.
            symmetry in HH.
            contradict HH.
            apply ZnotSucBN.
Qed.


(* 
  8- bbal t -> bbal (hr t)
     Precondiciones: bbal undefBTree, sale por allBal
*)
Lemma bal_hr: forall (t:BTree), bbal t -> bbal (hr t).
Proof.
intros t Hbal.
induction t.
- simpl.
  apply allBal.
- inversion Hbal.
  specialize (IHt1 H2). 
  specialize (IHt2 H3).
  destruct t1. 
  + simpl.
    assumption.
  + destruct t2.
    * assert(A1: bsize t1_1 ⊞ bsize t1_2 = Z).
      { simpl in H5.
        assert(U Z = sucBN Z). { reflexivity. }
        rewrite H6 in H5.
        apply lteqBN_suci in H5.
        inversion H5.
        * reflexivity.
        * inversion H7.
      }
       assert(A2: t1_1 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       assert(A3: t1_2 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       rewrite A2. rewrite A3.
       simpl.
       constructor.
       ** constructor.
       ** constructor.
       ** constructor.
       ** constructor. constructor.
    * assert(A1:  bsize (N a1 t1_1 t1_2) = bsize (N a2 t2_1 t2_2)
              \/  bsize (N a1 t1_1 t1_2) = sucBN (bsize (N a2 t2_1 t2_2))).
      { apply (bbalCond_eqs _ _ H4 H5). }
      destruct A1 as [A1|A1].
      {
        assert(A2:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2)) 
                  = U (bsize (N a1 t1_1 t1_2))).
        { assert(A3:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                    = sucBN (bsize (N a1 t1_1 t1_2) ⊞ bsize (N a2 t2_1 t2_2))).
          { reflexivity. }
          rewrite A3.
          rewrite A1.
          apply plus_U.
        }
        assert(A3:  hr (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                  = N a (N a1 t1_1 t1_2) (hr (N a2 t2_1 t2_2))).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3. 
        assert(A4: bsize (hr (N a2 t2_1 t2_2)) = predBN (bsize (N a2 t2_1 t2_2))).
        { apply bsize_hr.
           split.
           - simpl. apply ltBN_Z.
           - assumption.
        }
        constructor.
        ** assumption.
        ** assumption.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           apply lteqs.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           constructor.
      }
      {
        assert(A2:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2)) 
                  = D (bsize (N a2 t2_1 t2_2))).
        { assert(A3:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                    = sucBN (bsize (N a1 t1_1 t1_2) ⊞ bsize (N a2 t2_1 t2_2))).
          { reflexivity. }
          rewrite A3.
          rewrite A1.
          apply plus_D.
        }
        assert(A3:  hr (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                  = N a (hr (N a1 t1_1 t1_2)) (N a2 t2_1 t2_2)).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3.
        assert(A4: bsize (hr (N a1 t1_1 t1_2)) = predBN (bsize (N a1 t1_1 t1_2))).
        { apply bsize_hr.
           split.
           - simpl. apply ltBN_Z.
           - assumption.
        }
        constructor.
        ** assumption.
        ** assumption.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           constructor.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           apply lteqs.
      }
Qed.


(* 
  9- lookup_bn (hr t) j = lookup_bn t j
     Precondiciones: + bbal t
                     + sucBN j < bsize t
*)
Lemma hr_idx: forall (t: BTree) (j: BN),
  bbal t -> sucBN j <BN bsize t ->
  lookup_bn (hr t) j = lookup_bn t j.
Proof.
intros t.
induction t.
- intros j Hbal Hsize. 
  inversion Hsize.
- intros j Hbal Hsize. 
  remember (bsize (N a t1 t2)).
  assert(b <> Z).
  { rewrite Heqb. apply bsize_nonZ. intros HH. inversion HH. }
  destruct b.
  + contradict H. reflexivity.
  + destruct t1.
    * assert(t2 = E). 
      { apply (leftE_leaf E t2 a Hbal).
        reflexivity.
      }
      rewrite H0 in Heqb.
      simpl in Heqb.
      rewrite Heqb in Hsize.
      destruct j; inversion Hsize; inversion H3.
    * assert(A1:  hr (N a (N a0 t1_1 t1_2) t2)
                = N a (N a0 t1_1 t1_2) (hr t2)).
      { simpl. simpl in Heqb. rewrite <- Heqb. reflexivity. }
      rewrite A1.
      destruct j.
      ** reflexivity.
      ** reflexivity.
      ** simpl.
         apply IHt2.
         { inversion Hbal. assumption. }
         { inversion Hsize.
           assert(bsize t2 = b).
           { apply (prop_0_U a (N a0 t1_1 t1_2) t2).
             - assumption.
             - symmetry. assumption.
           }
          rewrite H3.
          assumption.
        }
  + destruct t1.
    * assert(t2 = E). 
      { apply (leftE_leaf E t2 a Hbal).
        reflexivity.
      }
      rewrite H0 in Heqb.
      simpl in Heqb.
      rewrite Heqb in Hsize.
      destruct j; inversion Hsize; inversion H3.
    * assert(A1:  hr (N a (N a0 t1_1 t1_2) t2)
                = N a (hr (N a0 t1_1 t1_2)) t2).
      { simpl. simpl in Heqb. rewrite <- Heqb. reflexivity. }
      rewrite A1.
      destruct j.
      ** reflexivity.
      ** simpl.
         apply IHt1.
         { inversion Hbal. assumption. }
         { inversion Hsize.
           assert(bsize (N a0 t1_1 t1_2) = sucBN b).
           { apply (prop_0_D a (N a0 t1_1 t1_2) t2).
             - assumption.
             - symmetry. assumption.
           }
          rewrite H3.
          apply ltBN_suc.
          assumption.
        }
      ** reflexivity.
Qed.


(* 
  10 - hr (he x t) = t.
       Precondiciones: + bbal t
*)
Lemma hr_he_inv: forall (t: BTree) (x:A),  bbal t -> hr (he x t) = t.
Proof.
intros t.
induction t.
- reflexivity.
- intros x Hbal.
  remember (bsize (N a t1 t2)).
  assert(b <> Z).
  { rewrite Heqb. apply bsize_nonZ. intros HH. inversion HH. }
  destruct b.
  + tauto.
  + simpl. simpl in Heqb. rewrite <- Heqb.
    assert(A1: D b = sucBN (sucBN (bsize t1) ⊞ bsize t2)).
    { rewrite <- plusSuc.
      rewrite <- Heqb.
      reflexivity.
    }
    assert(A2: hr (N a (he x t1) t2) = N a (hr (he x t1)) t2).
    { assert(A2 := bsize_he t1 x).
      simpl. rewrite A2. 
      rewrite <- A1.
      assert(he x t1 <> E).
      { intro HH. 
        rewrite HH in A2.
        simpl in A2.
        contradict A2.
        apply ZnotSucBN.
      }
      destruct (he x t1).
      - contradict H0. 
        reflexivity.
      - reflexivity.
    }
    rewrite A2.
    rewrite IHt1.
    * reflexivity.
    * inversion Hbal.
      assumption.
  + simpl. simpl in Heqb. rewrite <- Heqb.
    destruct t1.
    * assert(H2: t2 = E). 
      { apply (leftE_leaf E t2 a Hbal).
        reflexivity.
      }
      rewrite H2 in Heqb.
      simpl in Heqb.
      inversion Heqb.
    * assert(A1:  U (sucBN b) 
                = sucBN (sucBN (bsize (N a0 t1_1 t1_2)) ⊞ bsize t2)).
      { rewrite <- plusSuc.
        rewrite <- Heqb.
        reflexivity.
      }
      assert(A2:  hr (N a (N a0 t1_1 t1_2) (he x t2)) 
                = N a (N a0 t1_1 t1_2) (hr (he x t2))).
      { simpl. assert(A2 := bsize_he t2 x).
        simpl. rewrite A2. 
        rewrite <- plusSuc_2.
        rewrite plusSuc.
        simpl in A1.
        rewrite <- A1.
        assert(he x t2 <> E).
        { intro HH. 
          rewrite HH in A2.
          simpl in A2.
          contradict A2.
          apply ZnotSucBN.
        }
        destruct (he x t2).
        - contradict H0. 
          reflexivity.
        - reflexivity.
      }
      rewrite A2.
      rewrite IHt2.
      ** reflexivity.
      ** inversion Hbal.
         assumption.
Qed.


(* 
  11 - he (lookup t (predBN (bsize t))) (hr t) = t
       Precondicion: t tiene un elemento
                     bbal t
*)
Lemma he_hr_inv: forall (t:BTree), Z <BN bsize t -> bbal t ->
                 he (lookup_bn t (predBN (bsize t))) (hr t) = t.
Proof.
intros t.
induction t.
- intros H.
  inversion H.
- intros Hsize Hbal.
  destruct t1.
  + assert(H1: t2 = E). 
    { apply (leftE_leaf E t2 a Hbal).
      reflexivity.
    }
    rewrite H1 in *.
    reflexivity.
  + destruct t2.
    * assert(A1': E=E). {reflexivity. }
      assert(A1:= rightE a (N a0 t1_1 t1_2) E Hbal A1').
      destruct A1.
      { inversion H. }
      { destruct H as [aa H].
        inversion H.
        rewrite H2 in *.
        rewrite H3 in *.
        reflexivity.
      }
    * inversion Hbal.
      assert(A1 := bbalCond_eqs (bsize (N a0 t1_1 t1_2)) 
                                (bsize (N a1 t2_1 t2_2)) H4 H5).
      destruct A1 as [A1|A1].
      { assert(A2:  bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = U (bsize (N a0 t1_1 t1_2))).
        { simpl in A1. simpl. rewrite  A1. apply plus_U. }
        assert(A3:  hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = N a (N a0 t1_1 t1_2) (hr (N a1 t2_1 t2_2))).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3. clear A3.
        assert(A3:  predBN (bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                  = D (predBN (bsize (N a0 t1_1 t1_2)))).
        { simpl. 
          simpl in A1.
          rewrite A1.
          rewrite predsucBNinv.
          rewrite predsucBNinv.
          rewrite <- plusSuc_2.
          apply plus_D.
        }
        rewrite A3. clear A3.
        assert (A3:  lookup_bn (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                                 (D (predBN (bsize (N a0 t1_1 t1_2))))
                   = lookup_bn (N a1 t2_1 t2_2) 
                                 (predBN (bsize (N a0 t1_1 t1_2)))).
        { reflexivity. }
        rewrite A3. clear A3.
        remember (lookup_bn (N a1 t2_1 t2_2) (predBN (bsize (N a0 t1_1 t1_2)))) as x.
        assert(A3:  bsize (N a (N a0 t1_1 t1_2) (hr (N a1 t2_1 t2_2))) = 
                    D (predBN (bsize (N a0 t1_1 t1_2)))).
        { 
          assert(A3:= bsize_hr (N a1 t2_1 t2_2)).
          assert(A3': Z <BN bsize (N a1 t2_1 t2_2) /\ bbal (N a1 t2_1 t2_2)).
          { split.
            - simpl. apply ltBN_Z.
            - assumption.
          }
          specialize (A3 A3').
          simpl. simpl in A3. rewrite A3. 
          simpl in A1. rewrite A1. 
          rewrite predsucBNinv. 
          apply plus_D.
        }
        assert (A4:  he x (N a (N a0 t1_1 t1_2) (hr (N a1 t2_1 t2_2)))
                   = N a (N a0 t1_1 t1_2) (he x (hr (N a1 t2_1 t2_2)))).
        { simpl. simpl in A3. rewrite A3. reflexivity. }
        rewrite A4.
        rewrite A1 in Heqx. rewrite <- Heqx in IHt2.
        rewrite IHt2.
        - reflexivity.
        - simpl. apply ltBN_Z.
        - assumption.
      }
      { assert(A2:  bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = D (bsize (N a1 t2_1 t2_2))).
        { simpl in A1. simpl. rewrite  A1. apply plus_D. }
        assert(A3:  hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = N a (hr (N a0 t1_1 t1_2)) (N a1 t2_1 t2_2)).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3. clear A3.
        assert(A3:  predBN (bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                  = U (bsize (N a1 t2_1 t2_2))).
        { simpl. 
          simpl in A1.
          rewrite A1.
          rewrite predsucBNinv.
          rewrite <- plusSuc.
          apply plus_U.
        }
        rewrite A3. clear A3.
        assert (A3:  lookup_bn (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                                 (U (bsize (N a1 t2_1 t2_2)))
                   = lookup_bn (N a0 t1_1 t1_2) 
                                 (bsize (N a1 t2_1 t2_2))).
        { reflexivity. }
        rewrite A3. clear A3.
        remember (lookup_bn (N a0 t1_1 t1_2) (bsize (N a1 t2_1 t2_2))) as x.
        assert(A3:  bsize (N a (hr (N a0 t1_1 t1_2)) (N a1 t2_1 t2_2)) = 
                    U (bsize (N a1 t2_1 t2_2))).
        { 
          assert(A3:= bsize_hr (N a0 t1_1 t1_2)).
          assert(A3': Z <BN bsize (N a0 t1_1 t1_2) /\ bbal (N a0 t1_1 t1_2)).
          { split.
            - simpl. apply ltBN_Z.
            - assumption.
          }
          specialize (A3 A3').
          simpl. simpl in A3. rewrite A3. 
          simpl in A1. rewrite A1. 
          rewrite predsucBNinv. 
          apply plus_U.
        }
        assert (A4:  he x (N a (hr (N a0 t1_1 t1_2)) (N a1 t2_1 t2_2))
                   = N a (he x (hr (N a0 t1_1 t1_2))) (N a1 t2_1 t2_2)).
        { simpl. simpl in A3. rewrite A3. reflexivity. }
        rewrite A4.
        assert(A5: bsize (N a1 t2_1 t2_2) = predBN (bsize (N a0 t1_1 t1_2))).
        { rewrite A1. rewrite predsucBNinv. reflexivity. }
        rewrite <- A5 in IHt1.
        rewrite <- Heqx in IHt1.
        rewrite IHt1.
        - reflexivity.
        - simpl. apply ltBN_Z.
        - assumption.
      }
Qed.













