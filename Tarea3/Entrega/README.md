# Tarea 3 - Verificacion Formal

Nombre: Eduardo Pascual Aseff
Correo: eduardopascualaseff@gmail.com
Número de Cuenta: 520462566


## Distribución de los archivos

- El archivo Defs_BN.v contiene las definiciones de los números por paridad respectivamente, así como operaciones con estos como por ejemplo: sucesor, predecesor, orden, suma.

- El archivo Props_BN.v contiene propiedades que satisfacen los números por paridad. 

- El archivo Defs_BT.v contiene la definición de los arreglos extensibles utilizando árboles de Braun. Además, contiene la definición de las operaciones sobre este: búsqueda de elementos, extensión, etc.

- El archivo Props_BT.v contienen las propiedades e invariantes que satisfacen los arreglos extensibles definidos en el fichero Defs_BT.v


## Uso de la biblioteca

- Si desea utilizar solo los números por paridad y sus propiedades, debe importar a su proyecto el fichero Props_BN.v

- Si desea utilizar los arreglos extensibles, debe importar a su proyecto el fichero Props_BT.v

