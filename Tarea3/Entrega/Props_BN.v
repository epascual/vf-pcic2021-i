(* 
   Propiedades de Números binarios por paridad. 
   Copiado de los scripts de clase!
*)

Load Defs_BN.


(*****************************
 * Propiedades de BN         *
 *****************************)

Lemma dist_sym: forall (a b:BN), a <> b -> b <> a.
Proof.
  intros a b H1 H2. contradict H1. rewrite H2. reflexivity.
Qed.

Lemma UInj: forall (a b:BN), U a = U b -> a = b.
Proof.
intros.
inversion H.
trivial.
Qed.

Lemma DInj: forall (a b:BN), D a = D b -> a = b.
Proof.
intros.
inversion H.
trivial.
Qed.


Lemma ZnotU: forall (a:BN), Z <> U a.
Proof.
intros.
discriminate.
Qed.

Lemma ZnotD: forall (a:BN), Z <> D a.
Proof.
intros.
discriminate.
Qed.

  (* Lemma UnotD: forall (a:BN), U a <> D a. La cambié por la siguiente. C.V. 29/nov/2016 *)
Lemma UnotD: forall (a b:BN), U a <> D b.
Proof.
intros.
discriminate.
Qed.

Lemma DnotU: forall (a b:BN), D a <> U b. (* La agregué. C.V. 29/nov/2016 *)
Proof.
intros.
discriminate.
Qed.

Lemma bnotU : forall (a:BN), U a <> a.
Proof.
intros.
induction a.
(*a = Z*)
intuition.
inversion H.
(*a=U a*)
intuition.
inversion H.
apply IHa in H1.
trivial.
(*a=D a*)
intuition.
inversion H.
Qed.

Lemma bnotD : forall (a:BN), D a <> a.
Proof.
intros.
induction a.
(*a = Z*)
intuition.
inversion H.
(*a=U a*)
intuition.
inversion H.
(*a=D a*)
intuition.
inversion H.
apply IHa in H1.
trivial.
Qed.

Theorem dec_eq_BN: forall (a b:BN), {a = b} + {a <> b}.
Proof. (* This can be done fully automatic with decide equality *)
intro.
induction a.
- destruct b.
  + left. reflexivity.
  + right. apply ZnotU.
  + right. apply ZnotD.
- destruct b.
  + right. apply dist_sym. apply ZnotU.
  + specialize (IHa b).
    destruct IHa.
    * left. rewrite e. reflexivity.
    * right. intros H. contradict n. apply UInj. assumption.
  + right. apply UnotD.
- destruct b.
  + right. apply dist_sym. apply ZnotD.
  + right. apply DnotU.
  + specialize (IHa b).
    destruct IHa.
    * left. rewrite e. reflexivity.
    * right. intros H. contradict n. apply DInj. assumption.
Qed.





(***************************************
 * Propiedades de sucesor y predecesor *
 ***************************************)

Lemma ZnotSucBN: forall (a:BN), Z <> sucBN a.
Proof.
intros.
induction a.
simpl.
discriminate.
simpl.
discriminate.
simpl.
discriminate.
Qed.

Lemma notSucBN : forall (a:BN), a <> sucBN a.
Proof.
intros.
destruct a.
simpl; discriminate.
simpl; discriminate.
simpl; discriminate.
Qed.


Lemma bnNonZ: forall (b:BN), b <> Z -> exists (c:BN), b = U c \/ b = D c.
Proof.
intros.
destruct b.
intuition.
exists b;left;trivial.
exists b;right;trivial.
Qed.

Lemma predBNUD: forall (a:BN), a <> Z -> predBN (U a) = D (predBN a).
Proof.
intros.
destruct a.
contradict H.
trivial.
reflexivity.
reflexivity.
Qed.

Lemma U_not: forall (i j :BN), U i <> U j -> i <> j.
Proof.
intros.
contradict H.
apply f_equal.
trivial.
Qed.

Lemma D_not: forall (i j :BN), D i <> D j -> i <> j.
Proof.
intros.
contradict H.
apply f_equal.
trivial.
Qed.


Lemma predsucBNinv: forall (a:BN), predBN (sucBN a) = a.
Proof.
induction a.
- reflexivity.
- reflexivity.
- simpl.
  assert(A1:= ZnotSucBN a). 
  rewrite IHa.
  destruct (sucBN a).
  + contradict A1. tauto.
  + reflexivity.
  + reflexivity.
Qed.

Lemma sucpredBNinv: forall (a:BN), a <> Z -> sucBN (predBN a) = a.
Proof.
induction a.
- intros H. contradict H. reflexivity.
- intros H.
  destruct (a).
  + reflexivity.
  + simpl. simpl in IHa. rewrite IHa.
    * reflexivity.
    * intro. discriminate.
  + reflexivity.
- intros H. reflexivity.
Qed.




(*****************************
 * Propiedades de toN y toBN *
 *****************************)

Lemma toN_sucBN : forall (b : BN), toN(sucBN b) = S(toN b).
Proof.
induction b.
simpl.
trivial.

simpl.
ring.

simpl.
rewrite IHb.
ring.
Qed.

Lemma sucBN_toBN : forall (n : nat), sucBN (toBN n) = toBN (S n).
Proof.
destruct n.
simpl.
trivial.
simpl.
trivial.
Qed.

Lemma inverse_op : forall (n : nat), toN(toBN n) = n.
Proof.
induction n.
simpl.
trivial.
simpl.
rewrite toN_sucBN.
rewrite IHn.
trivial.
Qed.


Lemma SucBNinj: forall (a b : BN), sucBN a = sucBN b -> a = b.
Proof.
intros a.
induction a.
- intros b H.
  destruct b.
  + reflexivity.
  + simpl in H. inversion H. 
  + simpl in H. inversion H. contradict H1. apply ZnotSucBN.
- intros b H.
  destruct b.
  + simpl in H. inversion H. 
  + inversion H. reflexivity.
  + simpl in H. inversion H. 
- intros b H.
  destruct b.
  + simpl in H. inversion H. symmetry in H1. contradict H1. apply ZnotSucBN.
  + simpl in H. inversion H. 
  + simpl in H. inversion H. 
    rewrite (IHa b).
    * reflexivity.
    * assumption.
Qed.







(**************************
 * Propiedades de la suma *
 **************************)

Lemma plusBN_toN : forall (a b : BN), toN(a ⊞ b) = toN a + toN b.
Proof.
intro.
induction a.
simpl.
trivial.
intros.
destruct b.
simpl.
ring.
simpl.
rewrite IHa.
ring.
simpl.
rewrite toN_sucBN.
rewrite IHa.
ring.
destruct b.
simpl.
ring.
simpl.
rewrite toN_sucBN.
rewrite IHa.
ring.
simpl.
rewrite toN_sucBN.
rewrite IHa.
ring.
Qed.


Lemma plus_neutro: forall (b:BN), b ⊞ Z = b.
Proof.
intros.
destruct b.
simpl;trivial.
simpl;trivial.
simpl;trivial.
Qed.

Lemma plus_U: forall (b : BN),  sucBN (b ⊞ b) = U b.
Proof.
intros.
induction b.
simpl.
trivial.
simpl.
rewrite IHb.
trivial.
simpl.
rewrite IHb.
simpl.
trivial.
Qed.

Lemma plus_D: forall (b : BN),  sucBN (sucBN b ⊞ b) = D b.
Proof.
intros.
induction b.
simpl.
trivial.
simpl.
rewrite plus_U.
trivial.
simpl.
rewrite IHb.
trivial.
Qed.


Lemma plusSuc : forall (b c: BN), sucBN (b ⊞ c) = sucBN b ⊞ c.
Proof.
intros b.
induction b.
- intros. 
  destruct c.
  + reflexivity.
  + reflexivity.
  + reflexivity.
- intros.
  destruct c.
  + reflexivity.
  + reflexivity.
  + reflexivity.
- intros.
  destruct c.
  + reflexivity.
  + simpl. rewrite IHb. reflexivity.
  + simpl. rewrite IHb. reflexivity.
Qed.

Lemma plus_toBN:  forall (n m: nat), toBN(n + m) = toBN n ⊞ toBN m.
Proof.
intros.
induction n.
simpl.
trivial.
simpl.
rewrite IHn.
rewrite <- plusSuc.
trivial.
Qed.

Lemma suc_is_plusone: forall (b:BN), sucBN b = U Z ⊞ b.
Proof. 
destruct b. 
- reflexivity.
- reflexivity.
- reflexivity.
Qed.

Lemma inverse_op_2 : forall (b:BN), toBN(toN b) = b.
Proof.
induction b.
- reflexivity.
- simpl. rewrite plus_comm. 
  rewrite plus_toBN. rewrite plus_toBN. 
  rewrite plus_comm. 
  simpl (0 + toN b).  
  rewrite IHb. 
  assert(toBN 1 = U Z). reflexivity.
  rewrite H.
  rewrite <- suc_is_plusone.
  apply plus_U.
- simpl.
  rewrite plus_comm.
  rewrite (plus_comm (toN b) 0).
  simpl.
  rewrite plus_toBN.
  rewrite IHb.
  rewrite plusSuc.
  apply plus_D.
Qed.


Lemma plusComm: forall (a b:BN), (a ⊞ b) = (b ⊞ a).
Proof.
induction a.
- destruct b.
  + reflexivity.
  + reflexivity.
  + reflexivity.
- destruct b. 
  + reflexivity.
  + simpl. rewrite IHa. reflexivity.
  + simpl. rewrite IHa. reflexivity.
- destruct b. 
  + reflexivity.
  + simpl. rewrite IHa. reflexivity.
  + simpl. rewrite IHa. reflexivity.
Qed.

Lemma plusSuc_2 : forall (b c: BN), sucBN (b ⊞ c) = b ⊞ sucBN c.
Proof.
intros b c.
rewrite plusComm.
rewrite (plusComm b (sucBN c)).
apply plusSuc.
Qed.

Lemma plusBN_Z_Z: forall (x y:BN), x ⊞ y = Z -> x = Z /\ y = Z.
Proof.
destruct x.
- intros. simpl in H. tauto. 
- intros y H. symmetry in H. contradict H. 
  destruct y. 
  + simpl. apply ZnotU. 
  + simpl. apply ZnotD.
  + simpl. apply ZnotU.
- intros y H. symmetry in H. contradict H. 
  destruct y. 
  + simpl. apply ZnotD. 
  + simpl. apply ZnotU.
  + simpl. apply ZnotD.
Qed.


Lemma UDCase: forall (x:BN), x <> Z -> exists (b:BN), x = U b \/ x = D b.
Proof.
intros.
destruct x.
intuition.
exists x;left;trivial.
exists x;right;trivial.
Qed.

Lemma suc_not_zero: forall (x:BN), sucBN x <> Z.
Proof.
intros.
destruct x.
simpl;discriminate.
simpl;discriminate.
simpl;discriminate.
Qed.

Lemma addition_a_a : forall (a b:BN), a ⊞ a = b ⊞ b -> a = b.
Proof.
intros.
apply (f_equal sucBN) in H.
rewrite plus_U in H.
rewrite plus_U in H.
apply UInj.
trivial.
Qed.

Lemma addition_SucBNa_a : forall (a b:BN), sucBN a ⊞ a = sucBN b ⊞ b -> a = b.
Proof.
intros.
rewrite <- plusSuc in H.
rewrite <- plusSuc in H.
apply SucBNinj in H.
apply (f_equal sucBN) in H.
rewrite plus_U in H.
rewrite plus_U in H.
apply UInj.
trivial.
Qed.





(*************************
 * Propiedades del Orden *
 *************************)


Lemma ltBN_arefl: forall (a:BN), ~ a <BN a.
Proof.
intros.
induction a.
unfold not.
intros.
inversion H.
contradict IHa.
inversion IHa.
trivial.
contradict IHa.
inversion IHa.
trivial.
Qed.

Create HintDb PNatDb.

Hint Resolve ltBN_arefl: PNatDb.

Lemma ltBN_asym: forall (a b:BN), a <BN b -> ~ b <BN a.
Proof.
intros.
induction H.
- unfold not;intros.
  inversion H.
- unfold not;intros.
  inversion H.
- contradict IHltBN.
  inversion IHltBN.
  trivial.
- unfold not;intros.
  inversion H.
  apply (ltBN_arefl a).
  trivial.
(*exact (ltBN_arefl a H2).*)
- unfold not;intros.
  inversion H0.
  intuition.
- contradict IHltBN.
  inversion IHltBN.
  + rewrite H2 in H.
    trivial.
  + trivial.
- contradict IHltBN.
  inversion IHltBN.
  trivial.
Qed.

Hint Resolve ltBN_asym: PNatDb.

(*Lemma ltBN_antisym: forall (a b:BN), ltBN a b -> ltBN b a -> *)

Lemma ltBN_tr: forall (b c:BN), b <BN c -> forall (a:BN), a <BN b -> a <BN c.
Proof.
Admitted.

Hint Resolve ltBN_tr: PNatDb.


Lemma ltBN_trans: forall (a b c:BN), a <BN b -> b <BN c -> a <BN c.
Proof.
intros.
eapply ltBN_tr.
eexact H0.
trivial.
Qed.

Hint Resolve ltBN_trans: PNatDb.

Lemma lt_lteqBN_trans: forall (a b c:BN), a <BN b -> b ≤BN c -> a <BN c.
Proof.
intros.
inversion H0.
rewrite H2 in H.
trivial.
eapply ltBN_trans.
eexact H.
trivial.
Qed.

Hint Resolve lt_lteqBN_trans: PNatDb.

Lemma lteqBN_trans: forall (a b c:BN), a ≤BN b -> b ≤BN c -> a ≤BN c.
Proof.
intros.
inversion H.
trivial.
inversion H0.
rewrite H5 in H.
trivial.
constructor.
eapply ltBN_trans.
eexact H1.
trivial.
Qed.

Hint Resolve lteqBN_trans: PNatDb.

Lemma ltDs: forall (a:BN), (D a) <BN (U (sucBN a)).
Proof.
intros.
induction a.
simpl.
constructor.
constructor.
simpl.
constructor.
constructor.
simpl.
constructor.
trivial.
Qed.

Hint Resolve ltDs: PNatDb.

Lemma lts: forall (a:BN), a <BN (sucBN a).
Proof.
intros.
induction a.
constructor.
simpl.
constructor.
simpl.
constructor.
trivial.
Qed.

Hint Resolve lts: PNatDb.

Lemma lteqs: forall (a:BN), a ≤BN (sucBN a).
Proof.
intros.
induction a.
constructor.
constructor.
simpl.
constructor.
constructor.
simpl.
constructor.
constructor.
inversion IHa.
contradict H0.
apply notSucBN.
trivial.
Qed.

Hint Resolve lteqs: PNatDb.

Lemma ltpred : forall (a:BN), a <> Z -> (predBN a) <BN a.
Proof.
intros a H. 
induction a.
- contradict H. reflexivity.
- simpl. 
  destruct a. 
  + constructor.
  + constructor. apply IHa. 
    intros HH. symmetry in HH. contradict HH. apply ZnotU.
  + constructor. apply IHa. 
    intros HH. symmetry in HH. contradict HH. apply ZnotD.
- simpl. 
  constructor. 
Qed.


Hint Resolve ltpred: PNatDb.

Lemma lt1: forall (b a:BN), a <BN (sucBN b) -> a ≤BN b.
Proof.
Admitted.

Hint Resolve lt1: PNatDb.

Lemma lt2: forall (b a:BN), a ≤BN b -> a <BN (sucBN b).
Proof.
Admitted.

Hint Resolve lt2: PNatDb.

Lemma lteqBN_suc_pred : 
  forall (a b:BN), a <> Z -> a ≤BN (sucBN b) -> (predBN a) ≤BN b.
Proof.
intros.
assert ((predBN a) <BN a).
apply ltpred.
trivial.
assert (predBN a <BN sucBN b).
eapply lt_lteqBN_trans.
eexact H1.
trivial.
apply lt1.
trivial.
Qed.

Hint Resolve lteqBN_suc_pred: PNatDb.


Lemma ltaux1: forall (j:BN), Z ≤BN (D j) -> Z ≤BN j.
Proof.
Admitted.

Lemma lteqBN_refl : forall (b:BN), b ≤BN b.
Proof.
intros.
constructor.
Qed.

Lemma lteqBN_Z : forall (b:BN), Z ≤BN b.
Proof.
intros.
destruct b.
constructor.
constructor;constructor.
constructor;constructor.
Qed.

Theorem not_lt_suc: forall (a:BN), ~ exists (b:BN), a <BN b /\ b <BN (sucBN a).
Proof.
Admitted.



From Coq Require Import Classical.

Lemma trichotomy: forall (a b:BN), a <BN b \/ a=b \/ b <BN a.
Proof.
Admitted.


Lemma not_lt: forall (a b:BN), b ≤BN a -> ~ a <BN b.
Proof.
Admitted.

Lemma sucBN_lt: forall (a b:BN), sucBN a <> b -> a <BN b -> (sucBN a) <BN b.
Proof.
Admitted.


Lemma lt_suc_lteq: forall (a b:BN), a <BN b -> (sucBN a) ≤BN b.
Proof.
Admitted.

Lemma lteqBN_suc: forall (a b:BN), a ≤BN b -> (sucBN a) ≤BN (sucBN b). 
Proof.
intros.
inversion H.
constructor.
apply lt_suc_lteq.
apply lt2.
trivial.
Qed.

(* Next lemmas are used for Okasaki's size *)

Lemma lteqBN_U_U:forall (a b:BN), (U a) ≤BN (U b) -> a ≤BN b.
Proof.
intros.
inversion H.
constructor.
inversion H0.
constructor.
trivial.
Qed.

Lemma ltBN_U_U:forall (a b:BN), (U a) <BN (U b) -> a <BN b.
Proof.
intros.
inversion H.
assumption.
Qed.

Lemma lteqBN_D_D : forall (a b : BN), (D a) ≤BN (D b)-> a ≤BN b.
Proof.
intros.
inversion H.
constructor.
inversion H0.
constructor.
trivial.
Qed.

Lemma lteqBN_U_D:forall (a b:BN), (U a) ≤BN (D b) -> a ≤BN b.
Proof.
intros.
inversion H.
inversion H0.
constructor.
constructor.
trivial.
Qed.

Lemma lteqBN_D_U:forall (a b:BN), (D a) ≤BN (U b) -> a ≤BN b.
Proof.
intros.
inversion H.
inversion H0.
constructor.
trivial.
Qed.

Lemma bbalCond_eqs: forall (s t:BN), t ≤BN s -> s ≤BN sucBN t -> s = t \/ s = sucBN t.  (* nov-2016, C.V. *)
Proof.
intros.
inversion H.
intuition.
inversion H0.
intuition.
exfalso.
eapply not_lt_suc.
exists s.
split.
exact H1.
assumption.
Qed.


Lemma lt_U: forall (a b:BN), a <BN b <-> (U a) <BN U b.
Proof.
intros.
split.
- intros H. constructor. assumption.
- intros H. inversion H. assumption.
Qed.

Lemma lt_D: forall (a b:BN), a <BN b <-> (D a) <BN D b.
Proof.
intros.
split.
- intros H. constructor. assumption.
- intros H. inversion H. assumption.
Qed.





(**************************************************
 **************************************************
 **       Propiedades agregadas por mi           **
 **************************************************
 **************************************************)

Lemma ltBN_Z : forall (b:BN), Z <BN sucBN b.
Proof.
intros.
destruct b.
- constructor.
- constructor.
- constructor.
Qed.

Lemma ltBN_prop1: forall (a b:BN), a <BN b -> sucBN a = b \/ sucBN a <BN b.
Proof.
intros a b H.
assert(A1 := dec_eq_BN (sucBN a) b).
destruct A1 as [A1|A1].
- tauto.
- right. 
  apply sucBN_lt; assumption.
Qed. 

Lemma ltBN_suc: forall (a b:BN), a <BN b -> (sucBN a) <BN (sucBN b). 
Proof.
intros a b.
generalize dependent a.
induction b.
- intros a H.
  inversion H.
- intros a.
  induction a.
  + intros H.
    simpl.
    destruct b.
    * constructor.
    * constructor. constructor. 
    * constructor. constructor.
  + intros H.
    simpl. constructor.
    inversion H. assumption.
  + intros H.
    inversion H.
    simpl.
    assert(A1:= ltBN_prop1 a b H2).
    destruct A1 as [A1 | A1].
    * rewrite A1. 
      constructor.
    * constructor.
      assumption.
- intros a.
  induction a.
  + intros H.
    simpl.
    constructor.
    destruct b.
    * constructor.
    * constructor.
    * constructor.
  + intros H.
    simpl.
    constructor.
    inversion H.
    * apply lts.
    * apply (ltBN_trans _ b).
      ** assumption.
      ** apply lts.
  + intros H.
    simpl.
    constructor.
    apply IHb.
    inversion H.
    assumption.
Qed.

Lemma ltBN_suci: forall (a b:BN), (sucBN a) <BN (sucBN b) -> a <BN b. 
Proof.
intros.
assert(A1:= trichotomy a b).
destruct A1 as [A1|A1].
- assumption.
- exfalso. 
  destruct A1 as [A1|A1].
  + rewrite A1 in H. 
    contradict H.
    apply ltBN_arefl.
  + contradict H.
    apply ltBN_asym.
    apply ltBN_suc.
    assumption.
Qed.

Lemma lteqBN_suci: forall (a b:BN), (sucBN a) ≤BN (sucBN b) -> a ≤BN b. 
Proof.
intros.
inversion H.
- apply SucBNinj in H2.
  rewrite H2.
  constructor.
- constructor.
  apply ltBN_suci.
  assumption.
Qed.

Lemma bn_is0_or_gt0: forall (n : BN), Z = n \/ Z <BN n.
Proof.
intros n.
destruct n.
- left. reflexivity.
- right. constructor.
- right. constructor.
Qed.













