Load binTree.

Lemma mod_0_1 : forall (i:nat), modulo i 2 = 0 \/ modulo i 2 = 1.
Proof.
intro.
destruct i.
left.
trivial.
simpl.
destruct (snd (divmod i 1 0 0)).
right.
trivial.
left.
trivial.
Qed.

Theorem leftE_leaf_nat : forall (a : A) (t1 t2 : BTree),
       bal (N a t1 t2) -> t1 = E -> t2 = E.
Proof.
  intros a t1 t2 H Ht1.
  rewrite Ht1 in H.
  inversion H.
  apply size0_E.
  simpl in H5.
  lia.
Qed.

Lemma odd_index_btree : forall (i:nat) (a:A) (t1 t2:BTree), 
                         bal (N a t1 t2) -> i mod 2 = 1 -> 
                          i < size (N a t1 t2) -> i/2 < size t1.
Proof.
  intros i a t1 t2 Hbal Hmod Hsize.
  inversion Hbal.
  destruct i.
  { inversion Hmod. }
  { assert(i < size t1 + size t2). { simpl in Hsize. lia. }
    assert(i/2 = S i/2). 
    { (*porque S i es impar*) admit. }
    rewrite <- H7.
    assert(i < 2*size t1). { lia. }
    admit.
  }
Admitted.

Lemma even_index_btree : forall (i:nat) (a:A) (t1 t2:BTree), i<>0 -> 
                             bal (N a t1 t2) -> i mod 2 = 0 -> 
                              i < size (N a t1 t2) -> i/2 - 1 < size t2.
Proof.
Admitted.
(* 
- - - - - - - - - - - - - - - - - - - -
A call to lookup with index 'i' on a balanced Braun tree 't', 
that has just been updated with 'x' at same index 'i', 
returns 'x' itself
- - - - - - - - - - - - - - - - - - - -
*)
Lemma lkp_upd: forall (t:BTree) (x:A) (i:nat), bal t -> t <> E 
                                               -> i < size t -> lookup (upd t i x) i = x.
Proof.
intros t x.
induction t.  
- (*Base case t=E *)
intuition.
- (*Induction step t=N t1 t2*)
intros.
(*Cases on index*)
destruct (Nat.eq_dec i 0).
+ (*i=0*)
 rewrite e. 
 reflexivity.
+ (*i<>0*)
(*Cases on modulo i 2*) 
destruct (mod_0_1 i).
* (* i is even, i.e., modulo i 2 = 0*)
destruct (eq_btree_dec t2 E).
(* ==> Cases on t2 <== *)
-- (*i) t2 = E*)
assert (t2_E:=e).
destruct (rightE_nat a t1 t2).
++ trivial.
(* To use rightE_nat we need to prove t2 = E *)
++ trivial.
(*t1=E and t2=E *)
++ rewrite H3 in H1.
rewrite t2_E in H1.
simpl in H1.  Print lt.
inversion H1.
** (*Contradiction: i = 0*)
intuition.   Print le.
** (*Contradiction S i <= 0*)
inversion H5.
++ (*t1= N b E E and t2=E *)
rewrite t2_E in H1.
destruct H3.
rewrite H3 in H1.
simpl in H1.
inversion H1.
** (* i = 1 *)
rewrite H5 in H2.
simpl in H2.
inversion H2.
** (* i=0 *)
contradict n.  
inversion H5.
--- reflexivity. (* S i = 1 => i = 0 *)
--- inversion H7. (* S i <= 0 *)
--   
(* ii) t2<>E *)
replace (upd (N a t1 t2) i x) with (N a t1 (upd t2 (div i 2 - 1) x)).
replace (lookup (N a t1 (upd t2 (i / 2 - 1) x)) i) with (lookup (upd t2 (i / 2 - 1) x) (div i 2 - 1)).
apply IHt2.
inversion H.
trivial.
trivial.
(* As required by the inductive hypothesis, we need to prove that  i / 2 - 1 < size t2 *)
apply (even_index_btree i a t1 t2).
trivial.
trivial.
trivial.
trivial.
(* We neeed to prove the replace statement: lookup (upd t2 (i / 2 - 1) x) (i / 2 - 1) = lookup (N a t1 (upd t2 (i / 2 - 1) x)) i*)
simpl.
destruct i.
intuition.
simpl in H2.
simpl. (* divmod (S i) 1 0 1 *)
rewrite H2.
intuition.
(* We need to prove the replace statement: N a t1 (upd t2 (i / 2 - 1) x) = upd (N a t1 t2) i x*)
simpl.
simpl in H2.
rewrite H2.
simpl.
apply Nat.neq_0_r in n.
destruct n.
rewrite H3.
trivial.
* (* i is odd, i.e., modulo i 2 = 1*)
destruct (eq_btree_dec t1 E).
(* ==> Cases on t1 <== *)
-- (* i) t1 = E *)
assert (t1_E:= e).
apply (leftE_leaf_nat a t1 t2) in e.
(*t1=E and t2=E *)
++ rewrite e in H1.
rewrite t1_E in  H1.
simpl in H1.
inversion H1.
** (*Contradiction: i = 0*)
intuition.
(*Contradiction: S i <= 0*)
** inversion H4.
(* An assumption of leftE_leaf_nat is that bal(N a t1 t2) *)
++ trivial.
-- (* ii) t1<>E *)
replace (upd (N a t1 t2) i x) with (N a (upd t1 (div i 2) x) t2).
** replace (lookup (N a (upd t1 (i / 2) x) t2) i) with (lookup (upd t1 (i / 2) x) (div i 2)).
--- apply IHt1.
+++ inversion H.
trivial.
+++ trivial.
(* As required by the inductive hypothesis, we need to prove that i/2 < size t1*)
+++ apply (odd_index_btree i a t1 t2).
*** trivial.
*** trivial.
*** trivial.
---  (* We need to prove the replace statement: lookup (upd t1 (i / 2) x) (i / 2) = lookup (N a (upd t1 (i / 2) x) t2) i *)
simpl.
destruct i.
+++ intuition.
+++ simpl in H2.
simpl (divmod (S i) 1 0 1).
rewrite H2.
intuition.
**  (* We need to prove the replace statement: N a (upd t1 (i / 2) x) t2 = upd (N a t1 t2) i x *)
simpl.
simpl in H2.
rewrite H2.
apply Nat.neq_0_r in n.
destruct n.
rewrite H3.
trivial.
Qed.


