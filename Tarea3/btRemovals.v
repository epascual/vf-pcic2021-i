Load btExtensions.

(* Seccion de lemas auxiliares *)
Lemma ltBN_Z : forall (b:BN), Z <BN sucBN b.
Proof.
intros.
destruct b.
- constructor.
- constructor.
- constructor.
Qed.

Lemma ltBN_prop1: forall (a b:BN), a <BN b -> sucBN a = b \/ sucBN a <BN b.
Proof.
intros a b H.
assert(A1 := dec_eq_BN (sucBN a) b).
destruct A1 as [A1|A1].
- tauto.
- right. 
  apply sucBN_lt; assumption.
Qed. 

Lemma ltBN_suc: forall (a b:BN), a <BN b -> (sucBN a) <BN (sucBN b). 
Proof.
intros a b.
generalize dependent a.
induction b.
- intros a H.
  inversion H.
- intros a.
  induction a.
  + intros H.
    simpl.
    destruct b.
    * constructor.
    * constructor. constructor. 
    * constructor. constructor.
  + intros H.
    simpl. constructor.
    inversion H. assumption.
  + intros H.
    inversion H.
    simpl.
    assert(A1:= ltBN_prop1 a b H2).
    destruct A1 as [A1 | A1].
    * rewrite A1. 
      constructor.
    * constructor.
      assumption.
- intros a.
  induction a.
  + intros H.
    simpl.
    constructor.
    destruct b.
    * constructor.
    * constructor.
    * constructor.
  + intros H.
    simpl.
    constructor.
    inversion H.
    * apply lts.
    * apply (ltBN_trans _ b).
      ** assumption.
      ** apply lts.
  + intros H.
    simpl.
    constructor.
    apply IHb.
    inversion H.
    assumption.
Qed.

Lemma ltBN_suci: forall (a b:BN), (sucBN a) <BN (sucBN b) -> a <BN b. 
Proof.
intros.
assert(A1:= trichotomy a b).
destruct A1 as [A1|A1].
- assumption.
- exfalso. 
  destruct A1 as [A1|A1].
  + rewrite A1 in H. 
    contradict H.
    apply ltBN_arefl.
  + contradict H.
    apply ltBN_asym.
    apply ltBN_suc.
    assumption.
Qed.

Lemma lteqBN_suci: forall (a b:BN), (sucBN a) ≤BN (sucBN b) -> a ≤BN b. 
Proof.
intros.
inversion H.
- apply SucBNinj in H2.
  rewrite H2.
  constructor.
- constructor.
  apply ltBN_suci.
  assumption.
Qed.

Lemma bn_is0_or_gt0: forall (n : BN), Z = n \/ Z <BN n.
Proof.
intros n.
destruct n.
- left. reflexivity.
- right. constructor.
- right. constructor.
Qed.






Lemma le_not_E: forall (t: BTree) (x:A),
  le x t <> E.
Proof.
intros.
destruct t.
- intros H. 
  inversion H.
- intros H.
  inversion H.
Qed.

Lemma le_is_N: forall (t: BTree) (x:A),
  exists (t1 t2: BTree), le x t = N x t1 t2.
Proof.
intros.
destruct t.
- exists E.
  exists E.
  reflexivity.
- exists (le a t2).
  exists t1.
  reflexivity.
Qed.





(* 
  1- Implementacion de lr y de hr 
*)
Fixpoint lr (t1:BTree) : BTree :=
 match t1 with
  | E => undefBTree
  | N x l r => match l with
               | E => E
               | N y _ _ => N y r (lr l)
               end
 end.

Fixpoint hr (t1:BTree) : BTree  :=
 match t1 with
  | E => undefBTree
  | N x l r => match l with 
               | E => E
               | _ => match bsize t1 with 
                      | U b => N x l (hr r)
                      | D b => N x (hr l) r
                      |   Z => undefBTree 
                      end
               end
 end.


(* 
  2- bsize (lr t) = pred(bsize t) 
     Precondiciones: + t tiene un elemento
                     + t esta balanceado
*)
Lemma bsize_lr: forall (t:BTree), Z <BN bsize t /\ bbal t -> 
                bsize (lr t) = predBN (bsize t).
Proof.
intros t1 [Hsize HBal].
induction HBal.
- inversion Hsize. 
- destruct s.
  + simpl in H.
    assert(t = E).
    { apply bsize_Z.  
      inversion H. 
      - reflexivity.
      - inversion H1.
    }
    rewrite H1.
    reflexivity.
  + assert( lr (N a (N a0 s1 s2) t) = N a0 t (lr (N a0 s1 s2))).
    { reflexivity. }
    rewrite H1.
    assert(bsize (N a0 t (lr (N a0 s1 s2))) = 
           sucBN (bsize t ⊞ bsize (lr (N a0 s1 s2)) )).
    { reflexivity. }
    rewrite H2.
    rewrite IHHBal1.
    * simpl. 
      rewrite predsucBNinv.
      rewrite predsucBNinv.
      rewrite plusComm.
      rewrite plusSuc. 
      reflexivity.
    * simpl.
      apply ltBN_Z.
Qed.


(* 
  3- bbal t -> bbal (lr t)
     Precondiciones: bbal undefBTree, sale por allBal
*)
Lemma bal_lr: forall (t:BTree), bbal t -> bbal (lr t).
Proof.
intros t Hbal.
induction t.
- simpl.
  apply allBal.
- inversion Hbal.
  specialize (IHt1 H2). 
  specialize (IHt2 H3).
  destruct t1.
  + simpl. 
    assumption.
  + assert(A1: lr (N a (N a1 t1_1 t1_2) t2) = N a1 t2 (lr (N a1 t1_1 t1_2))).
    { reflexivity. }
    rewrite A1.
    constructor.
    * assumption.
    * apply IHt1.
    * rewrite bsize_lr.
      { apply lteqBN_suc_pred. 
        - simpl. 
          intros HH. symmetry in HH. contradict HH.
          apply ZnotSucBN.
        - assumption.
      }
      { split. 
        { simpl. apply ltBN_Z. }
        { assumption. }
      }
    * rewrite bsize_lr.
      { rewrite sucpredBNinv.
        - assumption.
        - intros HH. symmetry in HH. contradict HH.
          apply ZnotSucBN. 
      }
      { split. 
        { simpl. apply ltBN_Z. }
        { assumption. }
      }
Qed.


(* 
  4- lookup_bn (lr t) j = lookup_bn t (sucBN j)
     Precondiciones: + bbal t
                     + sucBN j < bsize t
*)
Lemma lr_idx: forall (t: BTree) (j: BN),
  bbal t -> sucBN j <BN bsize t ->
  lookup_bn (lr t) j = lookup_bn t (sucBN j).
Proof.
intros t.
induction t.
{ intros j _ Hsize. inversion Hsize. }
{ intros j Hbal Hsize.
  inversion Hbal.
  destruct j.
  { destruct t1; reflexivity. }
  { destruct t1. 
    { assert(A1: t2 = E).
      { apply bsize_Z. 
        inversion H4.
        - reflexivity.
        - inversion H6.
      }
      rewrite A1.
      reflexivity.
    }
    { reflexivity. }
  }
  {
    destruct t1.
    { reflexivity. }
    { assert(A1: lr (N a (N a1 t1_1 t1_2) t2) = 
                 N a1 t2 (lr (N a1 t1_1 t1_2))).
      { reflexivity. }
      rewrite A1. clear A1.
      assert(A2: lookup_bn (N a1 t2 (lr (N a1 t1_1 t1_2))) (D j) =
                 lookup_bn (lr (N a1 t1_1 t1_2)) j). 
      { reflexivity. }
      rewrite A2. clear A2.
      rewrite IHt1. 
      - reflexivity.
      - assumption.
      - clear IHt1. 
        clear IHt2. 
        assert(A1: bsize (N a (N a1 t1_1 t1_2) t2) =
                   sucBN (bsize (N a1 t1_1 t1_2) ⊞ bsize t2) ).
        { reflexivity. }
        rewrite A1 in Hsize. clear A1.
        assert(A1: D j <BN bsize (N a1 t1_1 t1_2) ⊞ bsize t2).
        { apply ltBN_suci. assumption. }
        assert(A3:  bsize (N a1 t1_1 t1_2) = bsize t2
                 \/  bsize (N a1 t1_1 t1_2) = sucBN (bsize t2)).
        { apply (bbalCond_eqs _ _ H4 H5). }
        destruct A3 as [A3 | A3].
        + rewrite <- A3 in A1.
          assert(A4: bsize (N a1 t1_1 t1_2) ⊞ bsize (N a1 t1_1 t1_2)
                   = D (predBN (bsize (N a1 t1_1 t1_2)))).
          { simpl. 
            rewrite predsucBNinv. 
            rewrite <- plus_D. 
            rewrite <- plusSuc_2. 
            reflexivity.
          }
          rewrite A4 in A1. clear A4.
          apply <- lt_D in A1.
          apply ltBN_suc in A1.
          rewrite sucpredBNinv in A1.
          * assumption.
          * simpl. 
            intros HH. symmetry in HH. contradict HH.
            apply ZnotSucBN.
        + rewrite A3 in A1.
          rewrite <- plusSuc in A1.
          rewrite plus_U in A1.
          inversion A1.
          apply ltBN_suc in H8.
          rewrite A3.
          assumption.
    }
  }
}
Qed.


(* 
  5 - lr (le x t) = t.
*)
Lemma lr_le_inv: forall (t: BTree) (x:A),  lr (le x t) = t.
Proof.
intros t.
induction t.
- reflexivity.
- intros. 
  assert(A1: le x (N a t1 t2) = N x (le a t2) t1). 
  { reflexivity. }
  rewrite A1. 
  assert(A2 := le_is_N t2 a).
  destruct A2 as [t3 [t4 A2]].
  simpl.
  rewrite IHt2.
  rewrite A2.
  reflexivity.
Qed.


(* 
  6 - le (lookup t Z) (lr t) = t
      Precondicion: t tiene un elemento
                    bbal t
*)
Lemma le_lr_inv: forall (t:BTree), Z <BN bsize t -> bbal t ->
                 le (lookup_bn t Z) (lr t) = t.
Proof.
intros t.
induction t.
- intros Hsize. inversion Hsize.
- intros Hsize Hbal.
  assert(A1:= bn_is0_or_gt0 (bsize t1)).
  destruct A1 as [A1 | A1].
  { assert(Ht1: t1 = E).
    { apply bsize_Z. rewrite A1. reflexivity. }
    assert(Ht2: t2 = E).
    { inversion Hbal.
      apply bsize_Z.
      rewrite <- A1 in H4.
      inversion H4.
      - reflexivity.
      - inversion H6.
    }
    rewrite Ht1.
    rewrite Ht2.
    reflexivity.
  }
  { inversion Hbal.
    specialize (IHt1 A1 H2).
    assert(A2: lookup_bn (N a t1 t2) Z = a).
    { reflexivity. }
    rewrite A2. clear A2.
    simpl.
    destruct t1.
    - inversion A1.
    - assert(A2: le a (N a1 t2 (lr (N a1 t1_1 t1_2)))
               = N a (le a1 (lr (N a1 t1_1 t1_2))) t2).
      { reflexivity. } 
      rewrite A2. clear A2.
      assert(A2: lookup_bn (N a1 t1_1 t1_2) Z = a1).
      { reflexivity. }
      rewrite A2 in IHt1.
      rewrite IHt1.
      reflexivity.
  }
Qed.

(* 
  7- bsize (hr t) = pred(bsize t) 
     Precondiciones: + t tiene un elemento
                     + t esta balanceado
*)
Lemma bsize_hr: forall (t:BTree), Z <BN bsize t /\ bbal t -> 
                bsize (hr t) = predBN (bsize t).
Proof.
intros t.
induction t.
- intros [H _].
  inversion H.
- intros [Hsize Hbal].
  destruct t1.
  + assert(H2: t2 = E). 
    { apply bsize_Z. 
      inversion Hbal.
      inversion H4.
      - reflexivity.
      - inversion H6.
    }
    rewrite H2.
    reflexivity.
  + assert(IH1_dep: Z <BN bsize (N a0 t1_1 t1_2) /\ bbal (N a0 t1_1 t1_2)).
    { split.
      - simpl.
        apply ltBN_Z.
      - inversion Hbal.
        assumption.
    }
    specialize (IHt1 IH1_dep). clear IH1_dep.
    destruct t2.
    ++ assert(A1: bsize t1_1 ⊞ bsize t1_2 = Z).
       { inversion Hbal. 
         simpl in H5.
         assert(U Z = sucBN Z). { reflexivity. }
         rewrite H6 in H5.
         apply lteqBN_suci in H5.
         inversion H5.
         * reflexivity.
         * inversion H7.
       }
       assert(A2: t1_1 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       assert(A3: t1_2 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       rewrite A2. rewrite A3.
       reflexivity.
    ++ assert(IH2_dep: Z <BN bsize (N a1 t2_1 t2_2) /\ bbal (N a1 t2_1 t2_2)).
       { split.
         - simpl.
           apply ltBN_Z.
         - inversion Hbal.
           assumption.
       } 
       specialize (IHt2 IH2_dep). clear IH2_dep.
       assert(A1:  predBN (bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                 = bsize (N a0 t1_1 t1_2) ⊞ bsize (N a1 t2_1 t2_2)).
       { simpl. rewrite predsucBNinv. reflexivity. }
       rewrite A1. clear A1.
       destruct (bbal_size_r a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)).
       * assert(A2:  bsize (hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                   = sucBN (bsize (N a0 t1_1 t1_2) ⊞ bsize (hr (N a1 t2_1 t2_2)))).
         { simpl in H.
           simpl. 
           rewrite H. 
           reflexivity.
         }
         rewrite A2.
         rewrite IHt2. 
         rewrite plusSuc_2.
         rewrite sucpredBNinv.
         ** reflexivity.
         ** simpl. 
            intros HH.
            symmetry in HH.
            contradict HH.
            apply ZnotSucBN.
       * assert(A2:  bsize (hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                   = sucBN (bsize (hr (N a0 t1_1 t1_2)) ⊞ bsize (N a1 t2_1 t2_2))).
         { simpl in H.
           simpl. 
           rewrite H. 
           reflexivity.
         }
         rewrite A2.
         rewrite IHt1.
         rewrite plusSuc.
         rewrite sucpredBNinv.
         ** reflexivity.
         ** simpl. 
            intros HH.
            symmetry in HH.
            contradict HH.
            apply ZnotSucBN.
Qed.


(* 
  8- bbal t -> bbal (hr t)
     Precondiciones: bbal undefBTree, sale por allBal
*)
Lemma bal_hr: forall (t:BTree), bbal t -> bbal (hr t).
Proof.
intros t Hbal.
induction t.
- simpl.
  apply allBal.
- inversion Hbal.
  specialize (IHt1 H2). 
  specialize (IHt2 H3).
  destruct t1. 
  + simpl.
    assumption.
  + destruct t2.
    * assert(A1: bsize t1_1 ⊞ bsize t1_2 = Z).
      { simpl in H5.
        assert(U Z = sucBN Z). { reflexivity. }
        rewrite H6 in H5.
        apply lteqBN_suci in H5.
        inversion H5.
        * reflexivity.
        * inversion H7.
      }
       assert(A2: t1_1 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       assert(A3: t1_2 = E).
       { apply plusBN_Z_Z in A1. apply bsize_Z. tauto. }
       rewrite A2. rewrite A3.
       simpl.
       constructor.
       ** constructor.
       ** constructor.
       ** constructor.
       ** constructor. constructor.
    * assert(A1:  bsize (N a1 t1_1 t1_2) = bsize (N a2 t2_1 t2_2)
              \/  bsize (N a1 t1_1 t1_2) = sucBN (bsize (N a2 t2_1 t2_2))).
      { apply (bbalCond_eqs _ _ H4 H5). }
      destruct A1 as [A1|A1].
      {
        assert(A2:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2)) 
                  = U (bsize (N a1 t1_1 t1_2))).
        { assert(A3:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                    = sucBN (bsize (N a1 t1_1 t1_2) ⊞ bsize (N a2 t2_1 t2_2))).
          { reflexivity. }
          rewrite A3.
          rewrite A1.
          apply plus_U.
        }
        assert(A3:  hr (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                  = N a (N a1 t1_1 t1_2) (hr (N a2 t2_1 t2_2))).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3. 
        assert(A4: bsize (hr (N a2 t2_1 t2_2)) = predBN (bsize (N a2 t2_1 t2_2))).
        { apply bsize_hr.
           split.
           - simpl. apply ltBN_Z.
           - assumption.
        }
        constructor.
        ** assumption.
        ** assumption.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           apply lteqs.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           constructor.
      }
      {
        assert(A2:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2)) 
                  = D (bsize (N a2 t2_1 t2_2))).
        { assert(A3:  bsize (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                    = sucBN (bsize (N a1 t1_1 t1_2) ⊞ bsize (N a2 t2_1 t2_2))).
          { reflexivity. }
          rewrite A3.
          rewrite A1.
          apply plus_D.
        }
        assert(A3:  hr (N a (N a1 t1_1 t1_2) (N a2 t2_1 t2_2))
                  = N a (hr (N a1 t1_1 t1_2)) (N a2 t2_1 t2_2)).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3.
        assert(A4: bsize (hr (N a1 t1_1 t1_2)) = predBN (bsize (N a1 t1_1 t1_2))).
        { apply bsize_hr.
           split.
           - simpl. apply ltBN_Z.
           - assumption.
        }
        constructor.
        ** assumption.
        ** assumption.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           constructor.
        ** rewrite A4.
           rewrite A1.
           simpl.
           rewrite predsucBNinv.
           apply lteqs.
      }
Qed.


(* 
  9- lookup_bn (hr t) j = lookup_bn t j
     Precondiciones: + bbal t
                     + sucBN j < bsize t
*)
Lemma hr_idx: forall (t: BTree) (j: BN),
  bbal t -> sucBN j <BN bsize t ->
  lookup_bn (hr t) j = lookup_bn t j.
Proof.
intros t.
induction t.
- intros j Hbal Hsize. 
  inversion Hsize.
- intros j Hbal Hsize. 
  remember (bsize (N a t1 t2)).
  assert(b <> Z).
  { rewrite Heqb. apply bsize_nonZ. intros HH. inversion HH. }
  destruct b.
  + contradict H. reflexivity.
  + destruct t1.
    * assert(t2 = E). 
      { apply (leftE_leaf E t2 a Hbal).
        reflexivity.
      }
      rewrite H0 in Heqb.
      simpl in Heqb.
      rewrite Heqb in Hsize.
      destruct j; inversion Hsize; inversion H3.
    * assert(A1:  hr (N a (N a0 t1_1 t1_2) t2)
                = N a (N a0 t1_1 t1_2) (hr t2)).
      { simpl. simpl in Heqb. rewrite <- Heqb. reflexivity. }
      rewrite A1.
      destruct j.
      ** reflexivity.
      ** reflexivity.
      ** simpl.
         apply IHt2.
         { inversion Hbal. assumption. }
         { inversion Hsize.
           assert(bsize t2 = b).
           { apply (prop_0_U a (N a0 t1_1 t1_2) t2).
             - assumption.
             - symmetry. assumption.
           }
          rewrite H3.
          assumption.
        }
  + destruct t1.
    * assert(t2 = E). 
      { apply (leftE_leaf E t2 a Hbal).
        reflexivity.
      }
      rewrite H0 in Heqb.
      simpl in Heqb.
      rewrite Heqb in Hsize.
      destruct j; inversion Hsize; inversion H3.
    * assert(A1:  hr (N a (N a0 t1_1 t1_2) t2)
                = N a (hr (N a0 t1_1 t1_2)) t2).
      { simpl. simpl in Heqb. rewrite <- Heqb. reflexivity. }
      rewrite A1.
      destruct j.
      ** reflexivity.
      ** simpl.
         apply IHt1.
         { inversion Hbal. assumption. }
         { inversion Hsize.
           assert(bsize (N a0 t1_1 t1_2) = sucBN b).
           { apply (prop_0_D a (N a0 t1_1 t1_2) t2).
             - assumption.
             - symmetry. assumption.
           }
          rewrite H3.
          apply ltBN_suc.
          assumption.
        }
      ** reflexivity.
Qed.


(* 
  10 - hr (he x t) = t.
       Precondiciones: + bbal t
*)
Lemma hr_he_inv: forall (t: BTree) (x:A),  bbal t -> hr (he x t) = t.
Proof.
intros t.
induction t.
- reflexivity.
- intros x Hbal.
  remember (bsize (N a t1 t2)).
  assert(b <> Z).
  { rewrite Heqb. apply bsize_nonZ. intros HH. inversion HH. }
  destruct b.
  + tauto.
  + simpl. simpl in Heqb. rewrite <- Heqb.
    assert(A1: D b = sucBN (sucBN (bsize t1) ⊞ bsize t2)).
    { rewrite <- plusSuc.
      rewrite <- Heqb.
      reflexivity.
    }
    assert(A2: hr (N a (he x t1) t2) = N a (hr (he x t1)) t2).
    { assert(A2 := bsize_he t1 x).
      simpl. rewrite A2. 
      rewrite <- A1.
      assert(he x t1 <> E).
      { intro HH. 
        rewrite HH in A2.
        simpl in A2.
        contradict A2.
        apply ZnotSucBN.
      }
      destruct (he x t1).
      - contradict H0. 
        reflexivity.
      - reflexivity.
    }
    rewrite A2.
    rewrite IHt1.
    * reflexivity.
    * inversion Hbal.
      assumption.
  + simpl. simpl in Heqb. rewrite <- Heqb.
    destruct t1.
    * assert(H2: t2 = E). 
      { apply (leftE_leaf E t2 a Hbal).
        reflexivity.
      }
      rewrite H2 in Heqb.
      simpl in Heqb.
      inversion Heqb.
    * assert(A1:  U (sucBN b) 
                = sucBN (sucBN (bsize (N a0 t1_1 t1_2)) ⊞ bsize t2)).
      { rewrite <- plusSuc.
        rewrite <- Heqb.
        reflexivity.
      }
      assert(A2:  hr (N a (N a0 t1_1 t1_2) (he x t2)) 
                = N a (N a0 t1_1 t1_2) (hr (he x t2))).
      { simpl. assert(A2 := bsize_he t2 x).
        simpl. rewrite A2. 
        rewrite <- plusSuc_2.
        rewrite plusSuc.
        simpl in A1.
        rewrite <- A1.
        assert(he x t2 <> E).
        { intro HH. 
          rewrite HH in A2.
          simpl in A2.
          contradict A2.
          apply ZnotSucBN.
        }
        destruct (he x t2).
        - contradict H0. 
          reflexivity.
        - reflexivity.
      }
      rewrite A2.
      rewrite IHt2.
      ** reflexivity.
      ** inversion Hbal.
         assumption.
Qed.


(* 
  11 - he (lookup t (predBN (bsize t))) (hr t) = t
       Precondicion: t tiene un elemento
                     bbal t
*)
Lemma he_hr_inv: forall (t:BTree), Z <BN bsize t -> bbal t ->
                 he (lookup_bn t (predBN (bsize t))) (hr t) = t.
Proof.
intros t.
induction t.
- intros H.
  inversion H.
- intros Hsize Hbal.
  destruct t1.
  + assert(H1: t2 = E). 
    { apply (leftE_leaf E t2 a Hbal).
      reflexivity.
    }
    rewrite H1 in *.
    reflexivity.
  + destruct t2.
    * assert(A1': E=E). {reflexivity. }
      assert(A1:= rightE a (N a0 t1_1 t1_2) E Hbal A1').
      destruct A1.
      { inversion H. }
      { destruct H as [aa H].
        inversion H.
        rewrite H2 in *.
        rewrite H3 in *.
        reflexivity.
      }
    * inversion Hbal.
      assert(A1 := bbalCond_eqs (bsize (N a0 t1_1 t1_2)) 
                                (bsize (N a1 t2_1 t2_2)) H4 H5).
      destruct A1 as [A1|A1].
      { assert(A2:  bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = U (bsize (N a0 t1_1 t1_2))).
        { simpl in A1. simpl. rewrite  A1. apply plus_U. }
        assert(A3:  hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = N a (N a0 t1_1 t1_2) (hr (N a1 t2_1 t2_2))).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3. clear A3.
        assert(A3:  predBN (bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                  = D (predBN (bsize (N a0 t1_1 t1_2)))).
        { simpl. 
          simpl in A1.
          rewrite A1.
          rewrite predsucBNinv.
          rewrite predsucBNinv.
          rewrite <- plusSuc_2.
          apply plus_D.
        }
        rewrite A3. clear A3.
        assert (A3:  lookup_bn (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                                 (D (predBN (bsize (N a0 t1_1 t1_2))))
                   = lookup_bn (N a1 t2_1 t2_2) 
                                 (predBN (bsize (N a0 t1_1 t1_2)))).
        { reflexivity. }
        rewrite A3. clear A3.
        remember (lookup_bn (N a1 t2_1 t2_2) (predBN (bsize (N a0 t1_1 t1_2)))) as x.
        assert(A3:  bsize (N a (N a0 t1_1 t1_2) (hr (N a1 t2_1 t2_2))) = 
                    D (predBN (bsize (N a0 t1_1 t1_2)))).
        { 
          assert(A3:= bsize_hr (N a1 t2_1 t2_2)).
          assert(A3': Z <BN bsize (N a1 t2_1 t2_2) /\ bbal (N a1 t2_1 t2_2)).
          { split.
            - simpl. apply ltBN_Z.
            - assumption.
          }
          specialize (A3 A3').
          simpl. simpl in A3. rewrite A3. 
          simpl in A1. rewrite A1. 
          rewrite predsucBNinv. 
          apply plus_D.
        }
        assert (A4:  he x (N a (N a0 t1_1 t1_2) (hr (N a1 t2_1 t2_2)))
                   = N a (N a0 t1_1 t1_2) (he x (hr (N a1 t2_1 t2_2)))).
        { simpl. simpl in A3. rewrite A3. reflexivity. }
        rewrite A4.
        rewrite A1 in Heqx. rewrite <- Heqx in IHt2.
        rewrite IHt2.
        - reflexivity.
        - simpl. apply ltBN_Z.
        - assumption.
      }
      { assert(A2:  bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = D (bsize (N a1 t2_1 t2_2))).
        { simpl in A1. simpl. rewrite  A1. apply plus_D. }
        assert(A3:  hr (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                  = N a (hr (N a0 t1_1 t1_2)) (N a1 t2_1 t2_2)).
        { simpl. simpl in A2. rewrite A2. reflexivity. }
        rewrite A3. clear A3.
        assert(A3:  predBN (bsize (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2)))
                  = U (bsize (N a1 t2_1 t2_2))).
        { simpl. 
          simpl in A1.
          rewrite A1.
          rewrite predsucBNinv.
          rewrite <- plusSuc.
          apply plus_U.
        }
        rewrite A3. clear A3.
        assert (A3:  lookup_bn (N a (N a0 t1_1 t1_2) (N a1 t2_1 t2_2))
                                 (U (bsize (N a1 t2_1 t2_2)))
                   = lookup_bn (N a0 t1_1 t1_2) 
                                 (bsize (N a1 t2_1 t2_2))).
        { reflexivity. }
        rewrite A3. clear A3.
        remember (lookup_bn (N a0 t1_1 t1_2) (bsize (N a1 t2_1 t2_2))) as x.
        assert(A3:  bsize (N a (hr (N a0 t1_1 t1_2)) (N a1 t2_1 t2_2)) = 
                    U (bsize (N a1 t2_1 t2_2))).
        { 
          assert(A3:= bsize_hr (N a0 t1_1 t1_2)).
          assert(A3': Z <BN bsize (N a0 t1_1 t1_2) /\ bbal (N a0 t1_1 t1_2)).
          { split.
            - simpl. apply ltBN_Z.
            - assumption.
          }
          specialize (A3 A3').
          simpl. simpl in A3. rewrite A3. 
          simpl in A1. rewrite A1. 
          rewrite predsucBNinv. 
          apply plus_U.
        }
        assert (A4:  he x (N a (hr (N a0 t1_1 t1_2)) (N a1 t2_1 t2_2))
                   = N a (he x (hr (N a0 t1_1 t1_2))) (N a1 t2_1 t2_2)).
        { simpl. simpl in A3. rewrite A3. reflexivity. }
        rewrite A4.
        assert(A5: bsize (N a1 t2_1 t2_2) = predBN (bsize (N a0 t1_1 t1_2))).
        { rewrite A1. rewrite predsucBNinv. reflexivity. }
        rewrite <- A5 in IHt1.
        rewrite <- Heqx in IHt1.
        rewrite IHt1.
        - reflexivity.
        - simpl. apply ltBN_Z.
        - assumption.
      }
Qed.


